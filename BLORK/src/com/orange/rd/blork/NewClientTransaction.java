/**
 *
 */
package com.orange.rd.blork;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import org.bson.Document;
import org.bson.types.Binary;
import java.util.Arrays;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BSONTransaction;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;
import com.orange.rd.blork.utils.PublicKey;
import com.orange.rd.blork.utils.Signature;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

/**
 * New client
 *
 * @author lsmv2780
 *
 */
public class NewClientTransaction extends BSONTransaction {

	private PublicKey clientPublicKey;
	private Signature clientSignature;
	private Signature authoritySignature;

	static final String CLI_PREFIX = "01";


	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#hydrate(java.nio.ByteBuffer)
	 */
	@Override
	public void hydrate(Document payload) throws DecodeError {
		if (payload.get("client_key") == null
				|| payload.get("client_sig") == null
				|| payload.get("issuer_sig") == null)
			throw new DecodeError("Malformed BSON document");


		try {
			clientPublicKey = new PublicKey(ByteBuffer.wrap(((Binary) payload.get("client_key")).getData()));
		} catch (InvalidKeySpecException e) {
			throw new DecodeError("the client publicKey is invalid");
		} catch (ClassCastException e) {
			throw new DecodeError("client public key should be binary : " + e.getMessage());
		}

		try {
			clientSignature = new Signature(ByteBuffer.wrap(((Binary) payload.get("client_sig")).getData()), clientPublicKey);
		} catch (ClassCastException e) {
			throw new DecodeError("client signature should be binary : " + e.getMessage());
		}

		try {
			authoritySignature = new Signature(ByteBuffer.wrap(((Binary) payload.get("issuer_sig")).getData()), Constants.getOperatorPublicKey());
		} catch (ClassCastException e) {
			throw new DecodeError("issuer signature should be binary : " + e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#toBSON()
	 */
	@Override
	public Document toBSON() {
		Document bson = new Document();
		bson.put("client_key", clientPublicKey.getInstance().getEncoded());
		bson.put("client_sig", clientSignature.getEncoded().array());
		bson.put("issuer_sig", authoritySignature.getEncoded().array());
		return bson;
	}

	@Override
	public void process(State state) throws BlorkError {
		String clientAddr = Constants.GLOBAL_NAMESPACE + CLI_PREFIX + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(clientPublicKey.getInstance().getEncoded(), clientPublicKey.getInstance().getEncoded().length - 31, clientPublicKey.getInstance().getEncoded().length)));
		try {
			ByteBuffer currentClient = state.getState(Collections.singleton(clientAddr)).get(clientAddr).asReadOnlyByteBuffer();
			currentClient.rewind();
			if (currentClient.remaining() > 0) {
				if (currentClient.get() != 0) throw new BlorkError("Client already exists");
			}
		} catch (InternalError e) {
			e.printStackTrace();
		} catch (InvalidTransactionException e) {
			e.printStackTrace();
			throw new BlorkError("Internal error : " + e.getMessage());
		}

		if (!clientSignature.isValid(ByteBuffer.wrap(clientPublicKey.getInstance().getEncoded()))) throw new BlorkError("Invalid signature");
		if (!authoritySignature.isValid(ByteBuffer.wrap(clientPublicKey.getInstance().getEncoded()))) throw new BlorkError("Invalid signature");

		try {
			state.setState(Collections.singleton(new SimpleEntry<String, ByteString>(clientAddr, ByteString.copyFrom(new byte[] {0x01}))));
		} catch (InternalError | InvalidTransactionException e ) {
			e.printStackTrace();
			throw new BlorkError("Error : "  + e.getMessage());
		}
	}

	@Override
	public int getId() {
		return 1;
	}

}
