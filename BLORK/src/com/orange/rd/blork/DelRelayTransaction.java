/**
 *
 */
package com.orange.rd.blork;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Collections;
import java.util.AbstractMap.SimpleEntry;

import org.bson.Document;
import org.bson.types.Binary;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BSONTransaction;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;
import com.orange.rd.blork.utils.PublicKey;
import com.orange.rd.blork.utils.Signature;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

/**
 * Delete a relay
 *
 * @author lsmv2780
 *
 */
public class DelRelayTransaction extends BSONTransaction {
	private static final String RLY_PREFIX = "02";
	private PublicKey relay;
	private Signature operator;
	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#hydrate(java.nio.ByteBuffer)
	 */
	@Override
	public void hydrate(Document payload) throws DecodeError {
		if (payload.get("relay_key") == null
				|| payload.get("issuer_sig") == null)
			throw new DecodeError("Malformed BSON document");


		try {
			relay = new PublicKey(ByteBuffer.wrap(((Binary) payload.get("relay_key")).getData()));
		} catch (InvalidKeySpecException e) {
			throw new DecodeError("the client publicKey is invalid");
		} catch (ClassCastException e) {
			throw new DecodeError("client public key should be binary : " + e.getMessage());
		}

		try {
			operator = new Signature(ByteBuffer.wrap(((Binary) payload.get("issuer_sig")).getData()), Constants.getOperatorPublicKey());
		} catch (ClassCastException e) {
			throw new DecodeError("issuer signature should be binary : " + e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#toBSON()
	 */
	@Override
	public Document toBSON() {
		Document bson = new Document();
		bson.put("relay_key", relay.getInstance().getEncoded());
		bson.put("issuer_sig", operator.getEncoded().array());
		return bson;
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#proccess(sawtooth.sdk.processor.State)
	 */
	@Override
	public void process(State state) throws BlorkError {
		String relayAddr = Constants.GLOBAL_NAMESPACE + RLY_PREFIX + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(relay.getInstance().getEncoded(), relay.getInstance().getEncoded().length - 31, relay.getInstance().getEncoded().length)));
		try {
			ByteBuffer currentRelay = state.getState(Collections.singleton(relayAddr)).get(relayAddr).asReadOnlyByteBuffer();

			currentRelay.rewind();
			if (currentRelay.remaining() != 1) throw new BlorkError("Error while processing transaction");
			else if (currentRelay.get() != 1) throw new BlorkError("Error while processing transaction");



			if (!operator.isValid(ByteBuffer.wrap(relay.getInstance().getEncoded()))) throw new BlorkError("Invalid  signature");

			ByteString newClientState = ByteString.copyFrom(new byte[] {0x00});
			state.setState(Collections.singleton(new SimpleEntry<String, ByteString>(relayAddr, newClientState)));
		} catch (InternalError | InvalidTransactionException e) {
			e.printStackTrace();
			throw new BlorkError("Internal error : " + e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#getId()
	 */
	@Override
	public int getId() {
		return 12;
	}

}
