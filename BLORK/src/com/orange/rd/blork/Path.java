/**
 *
 */
package com.orange.rd.blork;

import java.io.Serializable;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bson.BsonArray;
import org.bson.BsonBinary;
import org.bson.BsonBinaryReader;
import org.bson.BsonBinaryWriter;
import org.bson.BsonReader;
import org.bson.BsonSerializationException;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.io.BasicOutputBuffer;
import org.bson.io.OutputBuffer;
import org.bson.types.Binary;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;
import com.orange.rd.blork.utils.NonceHash;
import com.orange.rd.blork.utils.PathList;
import com.orange.rd.blork.utils.PublicKey;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

/**
 * reprsentation of a BLORK path
 * @author Vincent MESSIÉ
 *
 */
public class Path implements Iterable<PublicKey>, Serializable{


	/**
	 *
	 */
	private static final long serialVersionUID = 799013463510613271L;


	private PublicKey emitter;
	private PublicKey receiver;
	private PathList relays;
	private long dataExchanged;
	private boolean state;
	private BigInteger id;
	private int lastFrame=0;
	private long lastTime=0;

	/**
	 * Constructor
	 * @param  emitter    the emitter public key
	 * @param  receiver   the receiver public key
	 * @param  relays     the relay public keys, int emitter -> receiver order
	 * @param  state      Sawtooth current state
	 * @throws BlorkError if path is invalid, or if some key is not registered
	 */
	public Path(PublicKey emitter, PublicKey receiver, PathList relays, State state) throws BlorkError {
		//Auth
		if (!BlorkUtils.isClientRegistered(state, emitter))  throw new BlorkError("emitter not registered");
		if (!BlorkUtils.isClientRegistered(state, receiver)) throw new BlorkError("receiver not registered");

		for (PublicKey k : relays) {
			if (!BlorkUtils.isRelayRegistered(state, k)) throw new BlorkError("relay not registered");
		}

		this.emitter  = emitter;
		this.receiver = receiver;
		this.relays   = relays;

		this.state = true;
		this.dataExchanged = 0;

		this.id = calcId();

		Path possiblePath = getPathById(ByteBuffer.wrap(id.toByteArray()), state);

		if (possiblePath != null) {
			if (possiblePath.isActive()) throw new BlorkError("Duplicate path");
		}

		if (relays.contains(emitter) || relays.contains(receiver)) throw new BlorkError("Duplicate keys");


		saveToState(state);
	}

	/**
	 * Internal constructor (creating existing path from state)
	 * @param state         sawtooth State
	 * @param emitter       emitter public key
	 * @param receiver      receveir public key
	 * @param relays        relays public keys
	 * @param lastFrame     last frame executed
	 * @param dataExchanged data exchanged within the path
	 * @param status        path current status
	 * @param time          path last timestamp
	 */
	private Path(State state, PublicKey emitter, PublicKey receiver, PathList relays, int lastFrame, long dataExchanged, boolean status, long time) {
		this.emitter  = emitter;
		this.receiver = receiver;
		this.relays   = relays;

		this.state = status;
		this.dataExchanged = dataExchanged;

		this.id = calcId();

		this.lastFrame = lastFrame;
		this.lastTime  = time;
	}

	/**
	 * Get the path Id
	 * @return The calculated path Id (truncated SHA256 hash of all concatenated keys)
	 */
	public ByteBuffer getId() {
		return ByteBuffer.wrap(this.id.toByteArray());
	}

	/**
	 * Internal function for saving current instance in Sawtooth state
	 * @param  state      Sawtooth state
	 * @throws BlorkError if any errors occured during save operation
	 */
	private void saveToState(State state) throws BlorkError{
		Map<String, ByteString> toSave = new HashMap<String, ByteString>();
		Document pathAsBson = new Document();

		pathAsBson.put("emitter_key",  this.emitter.getInstance().getEncoded());
		pathAsBson.put("receiver_key", this.receiver.getInstance().getEncoded());

		BsonArray relayList = new BsonArray();

		for (PublicKey r : relays) {
			relayList.add(new BsonBinary(r.getInstance().getEncoded()));
		}

		pathAsBson.put("relay_keys", relayList);

		pathAsBson.put("status", this.state);
		pathAsBson.put("data_amount", dataExchanged);
		pathAsBson.put("last_pou", lastFrame);
		pathAsBson.put("last_timestamp", lastTime);

		DocumentCodec codec = new DocumentCodec();

		OutputBuffer output = new BasicOutputBuffer();
		BsonBinaryWriter writer = new BsonBinaryWriter(output);

		codec.encode(writer, pathAsBson, EncoderContext.builder().build());

		ByteBuffer tmp = ByteBuffer.allocate(31);
		tmp.put(getId());

		for (int k = 16; k < 31; k++)
			tmp.put((byte) 0xFF);

		tmp.rewind();
		String exceptedAddr  = Constants.GLOBAL_NAMESPACE + "03" + BlorkUtils.byteToHex(tmp);

		toSave.put(exceptedAddr, ByteString.copyFrom(output.toByteArray()));

		try {
			state.setState(toSave.entrySet());
		} catch (InternalError | InvalidTransactionException e) {
			throw new BlorkError("Exception when saving the path : " + e.getMessage());
		}
	}

	/**
	 * Check wether the current path is active or terminated
	 * @return true if active, false if terminated
	 */
	public boolean isActive() {
		return state;
	}

	/**
	 * Terminate the current path
	 * @param  s          Sawtooth state
	 * @throws BlorkError if any errors occured when saving to state
	 */
	public void terminate(State s) throws BlorkError {
		this.state = false;
		saveToState(s);
	}

	/**
	 * Add some more used data within the path (with a Proof of Use)
	 * @param  amount      amount of data consumed
	 * @param  frameNumber the related PoU frame number
	 * @param  time        Timestamp of add
	 * @param  state       current state
	 * @throws BlorkError  if anything is not logical (wrong frame number, of timestamp), or if the path is terminated
	 */
	public void putData(long amount, int frameNumber, long time, State state) throws BlorkError {
		if (!this.state) throw new BlorkError("this path is terminated");
		if (frameNumber != this.lastFrame + 1) { throw new BlorkError("Wrong frame number"); }
		if (time < this.lastTime) throw new BlorkError("Wrong timestamp");
		else this.lastFrame++;
		this.lastTime = time;
		this.dataExchanged += amount;

		saveToState(state);
	}

	/**
	 * Get the data exchanged within the path
	 * @return Data exchanged
	 */
	public long getDataExchanged() {
		return this.dataExchanged;
	}
	/**
	 * Get the path last PoU number
	 * @return The last path PoU number
	 */
	public int getLastFrame() {
		return this.lastFrame;
	}

	/**
	 * Get the path last PoU Timestamp
	 * @rreturn The last path PoU timestamp (POSIX64)
	 */
	public long getLastTimestamp() {
		return this.lastTime;
	}

	/**
	 * Internal function to calculate the path Id
	 * @return PathId as a big integer (to ensure equals function works properly)
	 */
	private BigInteger calcId() {
		int pSize = this.emitter.getInstance().getEncoded().length;

		for (PublicKey pk : this.relays) {
			pSize += pk.getInstance().getEncoded().length;
		}

		pSize += this.receiver.getInstance().getEncoded().length;

		ByteBuffer toHash = ByteBuffer.allocate(pSize);

		toHash.put(this.emitter.getInstance().getEncoded());

		for (PublicKey k : this.relays) {
			toHash.put(k.getInstance().getEncoded());
		}

		toHash.put(this.receiver.getInstance().getEncoded());

		ByteBuffer id = NonceHash.createFromPayload(toHash).getHash();

		return new BigInteger(Arrays.copyOfRange(id.array(), 0, 16));
	}

	/**
	 * Path iterator (iterate other all keys)
	 * @return path iterators
	 */
	@Override
	public Iterator<PublicKey> iterator() {
		return new PathIterator(this);
	}

	/**
	 * Get an existing path from state
	 * @param  pathId The Id of the path to look at
	 * @param  state  Sawtooth state
	 * @return        Path instance if exists, null otherwise (or if error has occured)
	 */
	public static Path getPathById(ByteBuffer pathId, State state) {
		if (pathId == null) return null;
		pathId.rewind();
		ByteBuffer tmp = ByteBuffer.allocate(31);
		tmp.put(pathId);

		for (int k = 16; k < 31; k++)
			tmp.put((byte) 0xFF);

		tmp.rewind();
		String exceptedAddr  = Constants.GLOBAL_NAMESPACE + "03" + BlorkUtils.byteToHex(tmp);


		Map<String, ByteString> result;
		try {
			result = state.getState(Collections.singleton(exceptedAddr));
		} catch (InternalError | InvalidTransactionException e1) {
			result = new HashMap<String,ByteString>();
			result.put(exceptedAddr, ByteString.EMPTY);
		}

		try {
			if (result == null) return null;
			ByteBuffer payload = result.getOrDefault(exceptedAddr, ByteString.EMPTY).asReadOnlyByteBuffer();

			if (payload.capacity() == 0) return null;

			BsonReader reader = new BsonBinaryReader(payload);

			DocumentCodec codec = new DocumentCodec();


			Document d  = codec.decode(reader, DecoderContext.builder().build());
			return createFromBSON(d, state);
		} catch (BsonSerializationException | NullPointerException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Internal function to create a Path from BSON State
	 * @param  d BSON Document
	 * @param  s Sawtooth state
	 * @return the corresponding instance
	 */
	private static Path createFromBSON(Document d, State s) {
		PathList rlys = new PathList();
		try {
			PublicKey emitter 	= new PublicKey(ByteBuffer.wrap(((Binary) d.get("emitter_key")).getData()));
			PublicKey receiver 	= new PublicKey(ByteBuffer.wrap(((Binary) d.get("receiver_key")).getData()));

			@SuppressWarnings("unchecked")
			List<Binary> rlyList = (List<Binary>) d.get("relay_keys");

			for (Binary v : rlyList) {
				rlys.add(new PublicKey(ByteBuffer.wrap(v.getData())));
			}

			boolean status 	= (boolean) d.get("status");
			long dataAmount	= (long)	d.get("data_amount");
			int lastPoU		= (int)		d.get("last_pou");
			long lastTime	= (long)	d.get("last_timestamp");

			Path path = new Path(s, emitter, receiver, rlys, lastPoU, dataAmount, status, lastTime);

			return path;

		} catch (InvalidKeySpecException | ClassCastException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof Path && ((Path) o).getId().equals(this.getId());
	}
	/**
	 * Iterator for the Path
	 */
	class PathIterator implements Iterator<PublicKey> {
		private Path path;
		private int index = 0;
		private int fullLength;
		public PathIterator(Path path) {
			this.path = path;
			this.fullLength = path.relays.size() + 2;

		}

		@Override
		public boolean hasNext() {

			return index >= 0 && index < fullLength;
		}

		@Override
		public PublicKey next() {
			if (index == 0) {
				index++;

				return path.emitter;
			} else if (index > 0 && index < fullLength - 1) {
				index++;
				return path.relays.get(index -2);
			} else if (index == fullLength - 1) {
				index++;
				return (path.receiver);
			} else {
				return null;
			}
		}
	}
	/**
	 * Get the emitter public key
	 * @return emitter public key
	 */
	public PublicKey getEmitter() {
		return this.emitter;
	}

	/**
	 * Get the receiver public key
	 * @return receiver public key
	 */
	public PublicKey getReceiver() {
		return this.receiver;
	}
	/**
	 * get the relays list
	 * @return relays list
	 */
	public PathList getRelays() {
		return this.relays;
	}
}
