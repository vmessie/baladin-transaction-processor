package com.orange.rd.blork;

import java.util.ArrayList;
import java.util.Collection;
import com.orange.rd.blork.utils.BSONTransaction;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.TransactionHandler;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import sawtooth.sdk.protobuf.TpProcessRequest;

/**
 * Blork Transaction handler
 * check and store PoU (encoded in BSON format) into the BLORKChain
 * @author Vincent MESSIÉ
 *
 */
public class TxHandler implements TransactionHandler {
	/**
	 * Constructor
	 */
	public TxHandler() {

		//Initialisation de toutes les transactions
		new DelClientTransaction();
		new DelRelayTransaction();
		new NewClientTransaction();
		new NewRelayTransaction();
		new NewPathTransaction();
		new DelPathTransaction();
		new PoUTransaction();
	}

	@Override
	public void apply(TpProcessRequest transactionRequest, State stateScore) throws InvalidTransactionException, InternalError {
		try {
			System.out.println("Nouvelle transaction");
			BSONTransaction.incomingTransaction(transactionRequest.getPayload().asReadOnlyByteBuffer(), stateScore);
			System.out.println("Transaction valide");
		} catch (DecodeError | BlorkError e) {
			System.out.println("Transaction en erreur");
			throw new InvalidTransactionException("Invalid Transaction : " + e.getMessage());
		}
	}

	@Override
	public Collection<String> getNameSpaces() {
		ArrayList<String> leRetour = new ArrayList<String>();
		leRetour.add(Constants.GLOBAL_NAMESPACE);
		return leRetour;
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	@Override
	public String transactionFamilyName() {
		return "blork";
	}
}
