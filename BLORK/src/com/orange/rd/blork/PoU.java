package com.orange.rd.blork;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Map;

import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.Nonce;
import com.orange.rd.blork.utils.NonceHash;
import com.orange.rd.blork.utils.PathList;
import com.orange.rd.blork.utils.PublicKey;
import com.orange.rd.blork.utils.Signature;

import sawtooth.sdk.processor.State;

/**
 * Representation of a PoU Tx
 *
 *
 * @author Vincent MESSIÉ
 *
 */
public class PoU implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 2599316445613628300L;

	private Path path;

	/**
	 * Proof of Use constructor
	 * @param  timestamp            Proof of use timestamp (POSIX64)
	 * @param  nextFrame            "padding" field
	 * @param  dataConsumption      data used from the last PoU
	 * @param  number               PoU number
	 * @param  pathId               Path ID
	 * @param  relays               List of relays
	 * @param  rlyNonce             relays nonces (in emitter -> receiver order)
	 * @param  rlySigOut            relays Signatures out  (in emitter -> receiver order)
	 * @param  rlySigBack           relays Signatures back (in emitter -> receiver order)
	 * @param  emitterNonce         the emitter nonce
	 * @param  emitterPK            the emitter public key
	 * @param  emitterSignatureOut  the first signature of the emitter
	 * @param  emitterSignatureBack the second signature of the emitter
	 * @param  receiverPK           the receiver public key
	 * @param  receiverSignature    the receiver signature
	 * @param  state                Sawtooth state
	 * @throws BlorkError           If the Proof of Use is invalid (wrong crytographic value, or path is illogical regarding the path of the PoU)
	 */
	public PoU(long timestamp, int nextFrame, long dataConsumption, int number, ByteBuffer pathId,PathList relays,
			Map<PublicKey, Nonce> rlyNonce, Map<PublicKey, Signature> rlySigOut,
			Map<PublicKey, Signature> rlySigBack, Nonce emitterNonce,
			PublicKey emitterPK, Signature emitterSignatureOut, Signature emitterSignatureBack,
			PublicKey receiverPK,Signature receiverSignature, State state) throws BlorkError {

		if (!relays.containsAll(rlyNonce.keySet())		|| relays.size() != rlyNonce.size())		throw new BlorkError("Wrong nonce");
		if (!relays.containsAll(rlySigOut.keySet())		|| relays.size() != rlySigOut.size())		throw new BlorkError("Wrong signature out");
		if (!relays.containsAll(rlySigBack.keySet())	|| relays.size() != rlySigBack.size())		throw new BlorkError("Wrong signature back");
		//Première PoU : number = 0, pathId = null
		//  data = 0
		//On commence par gérer le PathId


		if (!((number == 0 && pathId == null && dataConsumption == 0) || (number != 0 && pathId != null)))
			throw new BlorkError("Malformed PoB!");

		//Vérification du client
		ByteArrayOutputStream 	sigCheckByteArray = new ByteArrayOutputStream();
		DataOutputStream 		sigCheckPayload   = new DataOutputStream(sigCheckByteArray);

		boolean existingPath = false;

		ByteBuffer exceptedPath = calcId(emitterPK, relays, receiverPK);
		if (pathId != null) existingPath = true;
		if (pathId == null) pathId = exceptedPath;
		else if (!Arrays.equals(pathId.array(), exceptedPath.array())) throw new BlorkError("Wrong pathId");

		try {
			sigCheckPayload.writeInt(number);
			sigCheckPayload.writeLong(timestamp);
			if (existingPath) sigCheckPayload.write(pathId.array());
			else sigCheckPayload.write(new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00, 0x00});
			sigCheckPayload.writeInt(nextFrame);
			sigCheckPayload.writeLong(dataConsumption);
			sigCheckPayload.write(emitterPK.getInstance().getEncoded());
			sigCheckPayload.write(NonceHash.createFromPayload(emitterNonce.getNonceBytes()).getHash().array());

			if (!emitterSignatureOut.isValid(ByteBuffer.wrap(sigCheckByteArray.toByteArray()))) throw new BlorkError("Client out sig invalid");
			else sigCheckPayload.write(emitterSignatureOut.getEncoded().array());

			for (PublicKey rly : relays) {
				sigCheckPayload.write(rly.getInstance().getEncoded());
				NonceHash rlyHash = NonceHash.createFromPayload(rlyNonce.get(rly).getNonceBytes());
				sigCheckPayload.write(rlyHash.getHash().array());

				Signature s = rlySigOut.get(rly);
				if (!s.isValid(ByteBuffer.wrap(sigCheckByteArray.toByteArray()))) throw new BlorkError("Relay out sig invalid");
				else sigCheckPayload.write(s.getEncoded().array());
			}

			//Destinataire
			sigCheckPayload.write(receiverPK.getInstance().getEncoded());
			if (!receiverSignature.isValid(ByteBuffer.wrap(sigCheckByteArray.toByteArray()))) throw new BlorkError("Receiver sig invalid");
			else sigCheckPayload.write(receiverSignature.getEncoded().array());


			//Impossible d'utiliser Collections.reverse, à cause de la contrainte d'unicité
			for (int j = relays.size() - 1; j >=0; j--) {
				PublicKey rly = relays.get(j);
				sigCheckPayload.writeLong(rlyNonce.get(rly).getNonce());

				Signature s = rlySigBack.get(rly);
				if (!s.isValid(ByteBuffer.wrap(sigCheckByteArray.toByteArray()))) throw new BlorkError("Relay back sig invalid");
				else sigCheckPayload.write(s.getEncoded().array());
			}

			//émetteur : fin de la PoU

			sigCheckPayload.writeLong(emitterNonce.getNonce());

			if (!emitterSignatureBack.isValid(ByteBuffer.wrap(sigCheckByteArray.toByteArray()))) throw new BlorkError("Final emitter sig not valid");

			this.path = Path.getPathById(pathId, state);
			if (null == this.path) {
				this.path = new Path(emitterPK, receiverPK, relays, state);
			} else {
				this.path.putData(dataConsumption, number, timestamp, state);
			}

			//pas besoin d'attributs à priori


		} catch (IOException e) {
			e.printStackTrace();
			throw new BlorkError("IO Error");
		}


	}
	/**
	 * Get the PoU path
	 * @return PoU Path instance
	 */
	public Path getPath() {
		return this.path;
	}

	private ByteBuffer calcId(PublicKey emitter, PathList relays, PublicKey receiver) {
		int pSize = emitter.getInstance().getEncoded().length;

		for (PublicKey pk : relays) {
			pSize += pk.getInstance().getEncoded().length;
		}

		pSize += receiver.getInstance().getEncoded().length;

		ByteBuffer toHash = ByteBuffer.allocate(pSize);

		toHash.put(emitter.getInstance().getEncoded());

		for (PublicKey k : relays) {
			toHash.put(k.getInstance().getEncoded());
		}

		toHash.put(receiver.getInstance().getEncoded());

		ByteBuffer id = NonceHash.createFromPayload(toHash).getHash();

		return ByteBuffer.wrap(Arrays.copyOfRange(id.array(), 0, 16));
	}
}
