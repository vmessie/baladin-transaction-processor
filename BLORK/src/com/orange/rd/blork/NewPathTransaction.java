/**
 *
 */
package com.orange.rd.blork;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.lang.IndexOutOfBoundsException;

import org.bson.BsonSerializationException;
import org.bson.Document;
import org.bson.types.Binary;

import com.orange.rd.blork.utils.BSONTransaction;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.DecodeError;
import com.orange.rd.blork.utils.Nonce;
import com.orange.rd.blork.utils.PathList;
import com.orange.rd.blork.utils.PublicKey;
import com.orange.rd.blork.utils.Signature;

import sawtooth.sdk.processor.State;

/**
 *
 * New path
 *
 * @author lsmv2780
 *
 */
public class NewPathTransaction extends BSONTransaction {
	private PathList relays;
	private long timestamp;
	private int nextFrame;
	private long dataAmount;
	private int number;

	private Map<PublicKey, Nonce> rlyNonce;
	private Map<PublicKey, Signature> rlySigOut;
	private Map<PublicKey, Signature> rlySigBack;
	private Signature emitterSignatureBack;
	private Nonce emitterNonce;
	private PublicKey emitterPK;
	private PublicKey receiverPK;
	private Signature emitterSignatureOut;
	private Signature receiverSignature;

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#hydrate(java.nio.ByteBuffer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void hydrate(Document  d) throws DecodeError {
		try {
			emitterPK 	= new PublicKey(ByteBuffer.wrap(((Binary) d.get("emitter_key")).getData()));
			receiverPK	= new PublicKey(ByteBuffer.wrap(((Binary) d.get("receiver_key")).getData()));

			this.relays = new PathList();

			for (Binary w : (List<Binary>) d.get("relay_keys")) {
				this.relays.add(new PublicKey(ByteBuffer.wrap(w.getData())));
			}

			Document pou = (Document) d.get("init_pou");

			timestamp = (Long) pou.get("timestamp");
			number = (Integer) pou.get("number");
			nextFrame = (Integer) pou.get("next_frame");
			dataAmount = (Long) pou.get("data_amount");

			List<Long> rlyNonceArray 			= (List<Long>)   pou.get("nonces");
			List<Binary> rlySignatureOutArray 	= (List<Binary>) pou.get("sig_out");
			List<Binary> rlySignatureBackArray	= (List<Binary>) pou.get("sig_back");

			emitterNonce = new Nonce(rlyNonceArray.get(0));
			emitterSignatureOut 	= new Signature(ByteBuffer.wrap(rlySignatureOutArray.get(0).getData()), emitterPK);
			emitterSignatureBack	= new Signature(ByteBuffer.wrap(rlySignatureBackArray.get(0).getData()), emitterPK);

			receiverSignature	= new Signature(ByteBuffer.wrap(((Binary) pou.get("sig_receiver")).getData()), receiverPK);

			rlyNonce 	= new HashMap<>();
			rlySigOut	= new HashMap<>();
			rlySigBack	= new HashMap<>();

			int i = 1;
			for (PublicKey k : relays) {
				rlyNonce.put(k, new Nonce(rlyNonceArray.get(i)));
				rlySigOut.put(k, new Signature(ByteBuffer.wrap(rlySignatureOutArray.get(i).getData()), k));
				rlySigBack.put(k, new Signature(ByteBuffer.wrap(rlySignatureBackArray.get(i).getData()), k));
				i++;
			}
		} catch (InvalidKeySpecException e) {
			throw new DecodeError("a key is invalid");
		} catch (NullPointerException | ClassCastException | BsonSerializationException | IndexOutOfBoundsException e) {
			throw new DecodeError("Invalid BSON : " + e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#toBSON()
	 */
	@Override
	public Document toBSON() {
		Document ret = new Document();

		ret.put("emitter_key", emitterPK.getInstance().getEncoded());
		ret.put("receiver_key", receiverPK.getInstance().getEncoded());

		List<Binary> relayList = new ArrayList<Binary>();

		for (PublicKey k : relays) {
			relayList.add(new Binary(k.getInstance().getEncoded()));
		}

		ret.put("relay_keys", relayList);

		Document pou = new Document();

		pou.put("timestamp", timestamp);
		pou.put("data_amount", dataAmount);
		pou.put("next_frame", nextFrame);
		pou.put("number", number);

		List<Long> nonce = new ArrayList<Long>();
		List<Binary> sigOut = new ArrayList<Binary>();
		List<Binary> sigBack = new ArrayList<Binary>();

		nonce.add(emitterNonce.getNonce());
		sigOut.add(new Binary(emitterSignatureOut.getEncoded().array()));
		sigBack.add(new Binary(emitterSignatureBack.getEncoded().array()));


		for (PublicKey w : relays) {
			nonce.add(rlyNonce.get(w).getNonce());
			sigOut.add(new Binary(rlySigOut.get(w).getEncoded().array()));
			sigBack.add(new Binary(rlySigBack.get(w).getEncoded().array()));
		}

		pou.put("nonces", nonce);
		pou.put("sig_out", sigOut);
		pou.put("sig_back", sigBack);

		pou.put("sig_receiver", receiverSignature.getEncoded().array());

		ret.put("init_pou", pou);



		return ret;
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#proccess(sawtooth.sdk.processor.State)
	 */
	@Override
	public void process(State state) throws BlorkError {
		new PoU(timestamp, nextFrame, dataAmount,
			number, null, relays,
			rlyNonce, rlySigOut, rlySigBack,
			emitterNonce, emitterPK, emitterSignatureOut,
			emitterSignatureBack, receiverPK, receiverSignature, state);
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#getId()
	 */
	@Override
	public int getId() {
		return 3;
	}

}
