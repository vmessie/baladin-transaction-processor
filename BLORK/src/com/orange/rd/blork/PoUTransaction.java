/**
 *
 */
package com.orange.rd.blork;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.BsonArray;
import org.bson.BsonBinary;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.types.Binary;

import com.orange.rd.blork.utils.BSONTransaction;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.DecodeError;
import com.orange.rd.blork.utils.Nonce;
import com.orange.rd.blork.utils.PathList;
import com.orange.rd.blork.utils.PublicKey;
import com.orange.rd.blork.utils.Signature;

import sawtooth.sdk.processor.State;

/**
 *	Proof of use transaction
 *
 * @author lsmv2780
 *
 */
public class PoUTransaction extends BSONTransaction {

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#hydrate(java.nio.ByteBuffer)
	 */
	private PoU pou;

	private PublicKey emitterPK;
	private PublicKey receiverPK;
	private PathList relays;


	private Map<PublicKey, Nonce> rlyNonce;
	private Map<PublicKey, Signature> rlySigOut;
	private Map<PublicKey, Signature> rlySigBack;

	private Nonce emitterNonce;
	private Signature emitterSignatureOut;
	private Signature emitterSignatureBack;
	private Signature receiverSignature;

	private int number;
	private long timestamp;
	private long dataAmount;
	private int nextFrame;

	private ByteBuffer pathId;

	@SuppressWarnings("unchecked")
	@Override
	public void hydrate(Document d) throws DecodeError {
		try {
			try {
				emitterPK 	= new PublicKey(ByteBuffer.wrap(((Binary) d.get("emitter_key")).getData()));
				receiverPK	= new PublicKey(ByteBuffer.wrap(((Binary) d.get("receiver_key")).getData()));

				List<Binary> rlyArr = (List<Binary>) d.get("relay_keys");

				relays = new PathList();


				for (Binary w : rlyArr) {
					byte[] rlBin = w.getData();
					relays.add(new PublicKey(ByteBuffer.wrap(rlBin)));
				}


			} catch (InvalidKeySpecException e) {
				throw new DecodeError("Error while decoding : " + e.getMessage());
			}


			Document pou = (Document) d.get("pou");

			pathId = ByteBuffer.wrap(((Binary) pou.get("path_id")).getData());

			number		= (int)  pou.get("number");
			timestamp   = (long) pou.get("timestamp");
			dataAmount  = (long) pou.get("data_amount");
			nextFrame	= (int)  pou.get("next_frame");

			List<Long> rlyNonceArray 			= (List<Long>)   pou.get("nonces");
			List<Binary> rlySignatureOutArray 	= (List<Binary>) pou.get("sig_out");
			List<Binary> rlySignatureBackArray	= (List<Binary>) pou.get("sig_back");

			emitterNonce = new Nonce(rlyNonceArray.get(0));
			emitterSignatureOut 	= new Signature(ByteBuffer.wrap(rlySignatureOutArray.get(0).getData()), emitterPK);
			emitterSignatureBack	= new Signature(ByteBuffer.wrap(rlySignatureBackArray.get(0).getData()), emitterPK);

			receiverSignature	= new Signature(ByteBuffer.wrap(((Binary) pou.get("sig_receiver")).getData()), receiverPK);


			rlyNonce 	= new HashMap<>();
			rlySigOut	= new HashMap<>();
			rlySigBack	= new HashMap<>();

			int i = 1;
			for (PublicKey k : relays) {
				rlyNonce.put(k, new Nonce(rlyNonceArray.get(i)));
				rlySigOut.put(k, new Signature(ByteBuffer.wrap(rlySignatureOutArray.get(i).getData()), k));
				rlySigBack.put(k, new Signature(ByteBuffer.wrap(rlySignatureBackArray.get(i).getData()), k));
				i++;
			}
		} catch (NullPointerException | ClassCastException e) {
			throw new DecodeError("invalid BSON");
		}
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#toBSON()
	 */
	@Override
	public Document toBSON() {
		Document ret = new Document();

		ret.put("emitter_key", emitterPK.getInstance().getEncoded());
		ret.put("receiver_key", receiverPK.getInstance().getEncoded());

		List<Binary> relayList = new ArrayList<Binary>();

		for (PublicKey k : relays) {
			relayList.add(new Binary(k.getInstance().getEncoded()));
		}

		ret.put("relay_keys", relayList);

		Document pou = new Document();

		pou.put("path_id", pathId.array());
		pou.put("timestamp", timestamp);
		pou.put("data_amount", dataAmount);
		pou.put("next_frame", nextFrame);
		pou.put("number", number);

		List<Long> nonce = new ArrayList<Long>();
		List<Binary> sigOut = new ArrayList<Binary>();
		List<Binary> sigBack = new ArrayList<Binary>();

		nonce.add(emitterNonce.getNonce());
		sigOut.add(new Binary(emitterSignatureOut.getEncoded().array()));
		sigBack.add(new Binary(emitterSignatureBack.getEncoded().array()));


		for (PublicKey w : relays) {
			nonce.add(rlyNonce.get(w).getNonce());
			sigOut.add(new Binary(rlySigOut.get(w).getEncoded().array()));
			sigBack.add(new Binary(rlySigBack.get(w).getEncoded().array()));
		}

		pou.put("nonces", nonce);
		pou.put("sig_out", sigOut);
		pou.put("sig_back", sigBack);

		pou.put("sig_receiver", receiverSignature.getEncoded().array());

		ret.put("pou", pou);



		return ret;
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#proccess(sawtooth.sdk.processor.State)
	 */
	@Override
	public void process(State state) throws BlorkError {
		pou = new PoU(timestamp, nextFrame, dataAmount,
			number, pathId, relays,
			rlyNonce, rlySigOut, rlySigBack,
			emitterNonce, emitterPK, emitterSignatureOut,
			emitterSignatureBack, receiverPK, receiverSignature, state);

	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#getId()
	 */
	@Override
	public int getId() {
		return 10;
	}

}
