/**
 *
 */
package com.orange.rd.blork;

import sawtooth.sdk.processor.TransactionHandler;
import sawtooth.sdk.processor.TransactionProcessor;

/**
 * Main class
 *
 * @author lsmv2780
 *
 */
public class Main {

	/**
	 * BLORK transaction processor
	 * @param args arg should be the validator endpoint (default tcp://127.0.0.1:4004)
	 */
	public static void main(String[] args) {
		if (args.length == 0) args[0] = "tcp://127.0.0.1:4004";
		try {
			TransactionProcessor tp 	= new TransactionProcessor(args[0]);
			TransactionHandler handler	= new TxHandler();
			tp.addHandler(handler);

			Thread t = new Thread(tp);
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
