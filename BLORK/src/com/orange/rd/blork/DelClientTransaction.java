/**
 *
 */
package com.orange.rd.blork;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Collections;
import java.util.AbstractMap.SimpleEntry;

import org.bson.Document;
import org.bson.types.Binary;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BSONTransaction;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;
import com.orange.rd.blork.utils.PublicKey;
import com.orange.rd.blork.utils.Signature;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

/**
 * Delete a client
 *
 *
 * @author lsmv2780
 *
 */
public class DelClientTransaction extends BSONTransaction {
	private PublicKey clientPK;
	private Signature operator;

	static final String CLI_PREFIX = "01";
	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#hydrate(java.nio.ByteBuffer)
	 */
	@Override
	public void hydrate(Document payload) throws DecodeError {
		if (payload.get("client_key") == null
				|| payload.get("issuer_sig") == null)
			throw new DecodeError("Malformed BSON document");


		try {
			clientPK = new PublicKey(ByteBuffer.wrap(((Binary) payload.get("client_key")).getData()));
		} catch (InvalidKeySpecException e) {
			throw new DecodeError("the client publicKey is invalid");
		} catch (ClassCastException e) {
			throw new DecodeError("client public key should be binary : " + e.getMessage());
		}

		try {
			operator = new Signature(ByteBuffer.wrap(((Binary) payload.get("issuer_sig")).getData()), Constants.getOperatorPublicKey());
		} catch (ClassCastException e) {
			throw new DecodeError("issuer signature should be binary : " + e.getMessage());
		}

	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#toBSON()
	 */
	@Override
	public Document toBSON() {
		Document bson = new Document();
		bson.put("client_key", clientPK.getInstance().getEncoded());
		bson.put("issuer_sig", operator.getEncoded().array());
		return bson;
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#proccess(sawtooth.sdk.processor.State)
	 */
	@Override
	public void process(State state) throws BlorkError {
		String clientAddr = Constants.GLOBAL_NAMESPACE + CLI_PREFIX + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(clientPK.getInstance().getEncoded(), clientPK.getInstance().getEncoded().length - 31, clientPK.getInstance().getEncoded().length)));
		try {
			ByteBuffer currentClient = state.getState(Collections.singleton(clientAddr)).get(clientAddr).asReadOnlyByteBuffer();

			currentClient.rewind();
			if (currentClient.remaining() != 1) throw new BlorkError("Error while processing transaction");
			else if (currentClient.get() != 1) throw new BlorkError("Error while processing transaction");

			if (!operator.isValid(ByteBuffer.wrap(clientPK.getInstance().getEncoded()))) throw new BlorkError("Invalid signature");



			ByteString newClientState = ByteString.copyFrom(new byte[] {0x00});
			state.setState(Collections.singleton(new SimpleEntry<String, ByteString>(clientAddr, newClientState)));

		} catch (InternalError | InvalidTransactionException e) {
			e.printStackTrace();
			throw new BlorkError("Error :" + e.getMessage());
		}
	}

	@Override
	public int getId() {
		return 11;
	}

}
