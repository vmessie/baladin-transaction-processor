/**
 *
 */
package com.orange.rd.blork;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Collections;
import java.util.AbstractMap.SimpleEntry;

import org.bson.Document;
import org.bson.types.Binary;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BSONTransaction;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;
import com.orange.rd.blork.utils.PublicKey;
import com.orange.rd.blork.utils.Signature;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

/**
 * New relay
 *
 * @author lsmv2780
 *
 */
public class NewRelayTransaction extends BSONTransaction {
	private static final String RLY_PREFIX = "02";
	private PublicKey relayPublicKey;
	private Signature relaySignature;
	private Signature authoritySignature;
	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#hydrate(java.nio.ByteBuffer)
	 */
	@Override
	public void hydrate(Document payload) throws DecodeError {
		if (payload.get("relay_key") == null
				|| payload.get("relay_sig") == null
				|| payload.get("issuer_sig") == null)
			throw new DecodeError("Malformed BSON document");


		try {
			relayPublicKey = new PublicKey(ByteBuffer.wrap(((Binary) payload.get("relay_key")).getData()));
		} catch (InvalidKeySpecException e) {
			throw new DecodeError("the client publicKey is invalid");
		} catch (ClassCastException e) {
			throw new DecodeError("client public key should be binary : " + e.getMessage());
		}

		try {
			relaySignature = new Signature(ByteBuffer.wrap(((Binary) payload.get("relay_sig")).getData()), relayPublicKey);
		} catch (ClassCastException e) {
			throw new DecodeError("client signature should be binary : " + e.getMessage());
		}

		try {
			authoritySignature = new Signature(ByteBuffer.wrap(((Binary) payload.get("issuer_sig")).getData()), Constants.getOperatorPublicKey());
		} catch (ClassCastException e) {
			throw new DecodeError("issuer signature should be binary : " + e.getMessage());
		}

	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#toBSON()
	 */
	@Override
	public Document toBSON() {
		Document bson = new Document();
		bson.put("relay_key", relayPublicKey.getInstance().getEncoded());
		bson.put("relay_sig", relaySignature.getEncoded().array());
		bson.put("issuer_sig", authoritySignature.getEncoded().array());
		return bson;
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#proccess(sawtooth.sdk.processor.State)
	 */
	@Override
	public void process(State state) throws BlorkError {
		String clientAddr = Constants.GLOBAL_NAMESPACE + RLY_PREFIX + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(relayPublicKey.getInstance().getEncoded(), relayPublicKey.getInstance().getEncoded().length - 31, relayPublicKey.getInstance().getEncoded().length)));
		try {
			ByteBuffer currentClient = state.getState(Collections.singleton(clientAddr)).get(clientAddr).asReadOnlyByteBuffer();
			currentClient.rewind();
			if (currentClient.remaining() > 0) {
				if (currentClient.get() != 0) throw new BlorkError("Relay already exists");
			}
		} catch (InternalError e) {
			//OK
		} catch (InvalidTransactionException e) {
			e.printStackTrace();
			throw new BlorkError("An error has been thrown : " + e.getMessage());
		}

		if (!relaySignature.isValid(ByteBuffer.wrap(relayPublicKey.getInstance().getEncoded()))) throw new BlorkError("Invalid signature");
		if (!authoritySignature.isValid(ByteBuffer.wrap(relayPublicKey.getInstance().getEncoded()))) throw new BlorkError("Invalid signature");

		try {
			state.setState(Collections.singleton(new SimpleEntry<String, ByteString>(clientAddr, ByteString.copyFrom(new byte[] {0x01}))));
		} catch (InternalError | InvalidTransactionException e ) {
			e.printStackTrace();
			throw new BlorkError("Error : "  + e.getMessage());
		}
	}

	@Override
	public int getId() {
		return 2;
	}

}
