/**
 *
 */
package com.orange.rd.blork.utils;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

/**
 * Digital signature in BLORK (SHA256withRSA)
 *
 * @author lsmv2780
 *
 */
public class Signature {
	private PublicKey issuerKey;
	private java.security.Signature instance;
	private ByteBuffer signature;

	/**
	 * Constructor
	 * @param signature signature bytes
	 * @param pk        corresponding public key
	 */
	public Signature(ByteBuffer signature, PublicKey pk) {
		this.issuerKey = pk;
		try {
			this.instance = java.security.Signature.getInstance("SHA256withRSA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		this.signature = signature;
	}

	/**
	 * Check is the signature is valid
	 * @param  payload the data to check
	 * @return         true if signature match the data, false otherwise
	 */
	public boolean isValid(ByteBuffer payload) {
		try {
			instance.initVerify(issuerKey.getInstance());
			instance.update(payload);
			return instance.verify(signature.array());
		} catch (InvalidKeyException | SignatureException e) {
			return false;
		}
	}

	/**
	 * Get the associatied public key
	 * @return the associatied public key
	 */
	public PublicKey getPublicKey() {
		return this.issuerKey;
	}

	/**
	 * Get the java.security signature
	 * @return the java.security instance
	 */
	public java.security.Signature getInstance() {
		return this.instance;
	}

	/**
	 * Get the signature bytes
	 * @return the signature as a ByteBuffer
	 */
	public ByteBuffer getEncoded() {
		return this.signature;
	}
}
