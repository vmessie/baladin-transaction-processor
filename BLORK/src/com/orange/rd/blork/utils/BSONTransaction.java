/**
 *
 */
package com.orange.rd.blork.utils;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.bson.BsonBinaryReader;
import org.bson.BsonReader;
import org.bson.BsonSerializationException;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.DocumentCodec;

import sawtooth.sdk.processor.State;

/**
 * @author lsmv2780
 *
 * Abstract class for managing incoming transaction
 *
 */
public abstract class BSONTransaction {
	private static Map<Integer,BSONTransaction> idToTx = new HashMap<Integer, BSONTransaction>();
	/**
	 * protected method for hydrating the transaction object with its binary form
	 * @param bf the bytebuffer containing the object
	 * @throws DecodeError if payload is invalid
	 */
	public abstract void hydrate(Document d) throws DecodeError;
	/**
	 * Method for encoding the object to a payload
	 * @return the enconded payload
	 */
	public abstract Document toBSON();

	/**
	 * Proccess transaction
	 * @param state The current state of the ledger
	 * @throws BlorkError if failure
	 */
	public abstract void process(State state) throws BlorkError;

	/**
	 * Get Transaction ID
	 * @return the ID of the transaction
	 */
	public abstract int getId();

	/**
	 * Each transaction type should be registered
	 * This void ensure all is done.
	 */
	protected void registerTransaction() {
		if (idToTx.get(this.getId()) != null && idToTx.get(this.getId()) != this) throw new IllegalArgumentException(this.getId() + " already exists!");
		idToTx.put(this.getId(), this);
	}

	/**
	 * Default constructor
	 */
	public BSONTransaction() {
		this.registerTransaction();
	}

	/**
	 * Decode an transaction from BSON payload
	 * @param  payload     The payload to parse
	 * @param  state       Current state
	 * @throws DecodeError if BSON is invalid
	 * @throws BlorkError  If the required transaction subclass does not exist
	 */
	public static void incomingTransaction(ByteBuffer payload, State state) throws DecodeError, BlorkError {

		BsonReader reader = new BsonBinaryReader(payload);

		DocumentCodec codec = new DocumentCodec();
		Document d = null;

		try {
			d  = codec.decode(reader, DecoderContext.builder().build());
		} catch (BsonSerializationException e) {
			throw new DecodeError("Malformed BSON : " + e.getMessage());
		}

		if (d == null) throw new DecodeError("Malformed BSON");
		if (d.get("id") == null || d.get("body") == null) throw new DecodeError("Malformed BSON");

		int id = 0;
		try {
			id = (int) d.get("id");
		} catch (ClassCastException e) {
			e.printStackTrace();
			throw new DecodeError("Malformed BSON");
		}

		if (idToTx.get(id) == null) throw new DecodeError("This TX does not exists");
		else {
			BSONTransaction tx;
			try {
				tx = idToTx.get(id);
				tx.hydrate((Document) d.get("body"));
				tx.process(state);
			} catch (ClassCastException e) {
				e.printStackTrace();
				throw new DecodeError("MalformedBSON");
			}
		}
	}

	public static BSONTransaction getTransactionType(int id) {
		return idToTx.get(id);
	}
}
