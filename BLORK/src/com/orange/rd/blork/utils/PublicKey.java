/**
 *
 */
package com.orange.rd.blork.utils;

import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

/**
 * Class that represents a BLOKR publicKey (RSA2048)
 *
 * @author lsmv2780
 *
 */
public class PublicKey  {
	private java.security.PublicKey keyInstance;

	/**
	 * Constructor
	 * @param  payload                 The key bytes as ByteBuffer
	 * @throws InvalidKeySpecException is key is invalid
	 */
	public PublicKey(ByteBuffer payload) throws InvalidKeySpecException {
		X509EncodedKeySpec spec = new X509EncodedKeySpec(payload.array());
		try {
			KeyFactory kf = KeyFactory.getInstance("RSA");
			keyInstance = kf.generatePublic(spec);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the java.security instance
	 * @return the java instance
	 */
	public java.security.PublicKey getInstance() {
		return this.keyInstance;
	}

	@Override
	public boolean equals(Object o) {
		return o.getClass() == this.getClass() && Arrays.equals(((PublicKey) o).getInstance().getEncoded(), this.getInstance().getEncoded());
	}

}
