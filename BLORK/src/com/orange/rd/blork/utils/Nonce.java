/**
 *
 */
package com.orange.rd.blork.utils;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/**
 * Object for representation a PoU nonce
 * @author lsmv2780
 *
 */
public class Nonce {
	private long nonce;
	/**
	 * Constructor (from bytebuffer)
	 * @param  nonce                    nonce as byte buffer
	 * @throws BufferUnderflowException if the bytebuffer is too short
	 */
	public Nonce(ByteBuffer nonce) throws BufferUnderflowException{
		nonce.rewind();
		this.nonce = nonce.getLong();
	}

	/**
	 * Constructor (from long)
	 * @param nonce the nonce value
	 */
	public Nonce(long nonce) {
		this.nonce = nonce;
	}

	/**
	 * get nonce
	 * @return nonce as bytebuffer
	 */
	public ByteBuffer getNonceBytes() {
		ByteBuffer retour = ByteBuffer.allocate(8);
		retour.putLong(nonce);
		return retour;
	}

	/**
	 * get nonce
	 * @return nonce as long
	 */
	public long getNonce() {
		return this.nonce;
	}
}
