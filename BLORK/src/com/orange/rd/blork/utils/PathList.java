/**
 *
 */
package com.orange.rd.blork.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * class for reprensentating a list of relay
 *
 * implements both List interface, and Set interface
 * @author lsmv2780
 *
 */
public class PathList extends ArrayList<PublicKey> implements Set<PublicKey>, Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 2876839759300759051L;

	/**
	 * Constructor
	 * @param  c                        from collection
	 * @throws IllegalArgumentException if collection is invalid (duplicate element)
	 */
	public PathList(Collection<PublicKey> c) throws IllegalArgumentException {
		super(c);
		List<PublicKey> forCheck = new ArrayList<PublicKey>();
		for (PublicKey key : c) {
			if (forCheck.contains(key)) throw new IllegalArgumentException();
			else forCheck.add(key);
		}
	}

	/**
	 * Constructor
	 */
	public PathList() {
		super();
	}

	@Override
	public boolean add(PublicKey s) {
		if(this.contains(s)) return false;
		else return super.add(s);
	}

	@Override
	public void add(int pos, PublicKey s) throws IllegalArgumentException {
		if (this.contains(s)) throw new IllegalArgumentException();
		super.add(pos,  s);
	}

	@Override
	public boolean addAll(Collection<? extends PublicKey> c) {
		boolean ok = false;
		for (PublicKey e : c) {
			ok = this.add(e);
		}
		return ok;
	}

	@Override
	public boolean addAll(int pos, Collection<? extends PublicKey> c) {
		boolean ok = false;
		int currentPos = pos;
		for (PublicKey e : c) {
			try {
				this.add(currentPos, e);
				currentPos++;
				ok = true;
			} catch(IllegalArgumentException e1) {
				//rien à faire
			}
		}

		return ok;
	}
	@Override
	public PublicKey set(int pos, PublicKey e) throws IllegalArgumentException {
		if (this.contains(e) && this.indexOf(e) != pos) throw new IllegalArgumentException();
		return super.set(pos, e);
	}
}
