package com.orange.rd.blork.utils;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

public class Constants {
	public static final byte TYPE_NEW_CLIENT	= 0x01;
	public static final byte TYPE_NEW_RELAY		= 0x02;
	public static final byte TYPE_NEW_PATH		= 0x03;

	public static final byte TYPE_DEL_CLIENT	= 0x0B;
	public static final byte TYPE_DEL_RELAY		= 0x0C;
	public static final byte TYPE_STOP_PATH		= 0x0D;

	public static final byte TYPE_PoU			= 0x0A;


	public static final int SIZE_LONG			= 8;
	public static final int SIZE_PATH_ID_		= 16;
	public static final int SIZE_NEXT_FRAME_	= 4;
	public static final int SIZE_DATA_CONSUMED_	= 8;

	public static final int SIZE_NONCE			= 8;
	public static final int SIZE_NONCE_HASH		= 32;


	public static final String GLOBAL_NAMESPACE = "313433";

	public static final ByteBuffer operatorPublicKey = ByteBuffer.wrap(
			Base64.getDecoder().decode("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgkjJ78bcId0WVp8tBSuV" +
					"bidO1v57So4aNCbBDaefSY8wawmUYodXYG3aXWhjYBa26ioHcEBiLf/cFJ3560rG" +
					"jxUz83g36Hpqo5FeBzqQZoTLurFqyHFrE7vggbnIYJy/JDRwGy20mBa6dDp2BeNR" +
					"ICi+OxpHJwfv1KY65sH6p8gHcVONWK0QhhsTUvVFHxuaf0eJZTmtVyWWccYoS/gw" +
					"DKmGDcDafhJ3shwdqBqHrPG/tGpIjRlS5o5DBhcbEa1lfLJohSeQKpwUOp70qddO" +
					"aanVVWKYOQd8hO2H5aW/LvKzVYxYhoImbI7MhWjuDhoEaBHjiNvhKfDwzq5HxyXd" +
					"RQIDAQAB"));

	/**
	 * Get operator public key
	 * @return [description]
	 */
	public static final PublicKey getOperatorPublicKey() {
		try {
			return new PublicKey(operatorPublicKey);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
			return null;
		}
	}

}
