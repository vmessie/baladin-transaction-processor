/**
 *
 */
package com.orange.rd.blork.utils;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author lsmv2780
 *
 */

/**
 * Class for representating a PoU nonce hash (and more generally any SHA256 hash)
 */
public class NonceHash {
	private byte[] hash;
	/**
	 * Constructor
	 * @param hash hash bytes
	 */
	public NonceHash(ByteBuffer hash) {
		this(hash.array());
	}
	/**
	 * Constructor
	 * @param hash hash bytes
	 */
	public NonceHash(byte[] hash) {
		this.hash = hash;
	}

	/**
	 * Check the validity of a hash
	 * @param  payload the data to check
	 * @return         true is hash matches the data, false otherwise
	 */
	public boolean isHashValid(ByteBuffer payload) {
		NonceHash second = createFromPayload(payload);
		return second.equals(this);
	}

	/**
	 * Get the current hash bytes
	 * @return hash as a bytebuffer
	 */
	public ByteBuffer getHash() {
		return ByteBuffer.wrap(hash);
	}

	/**
	 * Create a hash from a data payload
	 * @param  payload the payload to hash
	 * @return         the corresponding NonceHash instance
	 */
	static public NonceHash createFromPayload(ByteBuffer payload) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			return new NonceHash(digest.digest(payload.array()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public boolean equals(Object o) {
		return o.getClass() == NonceHash.class && Arrays.equals(this.hash, ((NonceHash) o).hash);
	}
}
