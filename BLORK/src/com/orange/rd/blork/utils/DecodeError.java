/**
 *
 */
package com.orange.rd.blork.utils;

/**
 * @author lsmv2780
 *
 */
public class DecodeError extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = -1382916494309788891L;
	/**
	 * Error to be thrown when there is a BSON encoding issue
	 * @param msg The error message
	 */
	public DecodeError(String msg) {
		super(msg);
	}
}
