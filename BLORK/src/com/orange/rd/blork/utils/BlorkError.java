package com.orange.rd.blork.utils;

public class BlorkError extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * General error in application
	 * @param msg error description
	 */
	public BlorkError(String msg) {
		super(msg);
	}
}
