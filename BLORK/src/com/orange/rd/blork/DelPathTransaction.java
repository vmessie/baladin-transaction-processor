/**
 *
 */
package com.orange.rd.blork;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import org.bson.Document;
import org.bson.types.Binary;

import com.orange.rd.blork.utils.BSONTransaction;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.DecodeError;
import com.orange.rd.blork.utils.PublicKey;
import com.orange.rd.blork.utils.Signature;

import sawtooth.sdk.processor.State;

/**
 *
 * Delete a path
 * @author lsmv2780
 *
 */
public class DelPathTransaction extends BSONTransaction {
	private ByteBuffer pathID;
	private Signature deleter;
	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#hydrate(java.nio.ByteBuffer)
	 */
	@Override
	public void hydrate(Document d) throws DecodeError {
		try {
			pathID = ByteBuffer.wrap(((Binary) d.get("path_id")).getData());

			ByteBuffer author 	= ByteBuffer.wrap(((Binary) d.get("author_key")).getData());
			ByteBuffer sig		= ByteBuffer.wrap(((Binary) d.get("author_sig")).getData());


			deleter = new Signature(sig, new PublicKey(author));
		} catch (InvalidKeySpecException e) {
			throw new DecodeError("key invalid");
		} catch (NullPointerException | ClassCastException e) {
			throw new DecodeError("Invalid BSON : " + e.getMessage());
		}

	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#toBSON()
	 */
	@Override
	public Document toBSON() {
		Document ret = new Document();
		ret.put("path_id", pathID.array());
		ret.put("author_key",
				deleter.getPublicKey().getInstance().getEncoded());
		ret.put("author_sig", deleter.getEncoded().array());

		return ret;
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#proccess(sawtooth.sdk.processor.State)
	 */
	@Override
	public void process(State state) throws BlorkError {
		ByteBuffer toVerify = ByteBuffer.allocate(
				pathID.capacity()
				+ deleter.getPublicKey().getInstance()
					.getEncoded().length);

		toVerify.put(pathID);
		toVerify.put(deleter.getPublicKey().getInstance()
				.getEncoded());

		toVerify.rewind();

		if (!deleter.isValid(toVerify)) {
			throw new BlorkError("Invalid signature");
		} else {
			Path toDelete = Path.getPathById(pathID, state);
			if (toDelete == null) throw new BlorkError("Path does not exist");
			if (toDelete.isActive() == false) throw new BlorkError("Path was already inactive");

			PublicKey author = deleter.getPublicKey();

			if (toDelete.getEmitter().equals(author)
			   || toDelete.getReceiver().equals(author)
			   || toDelete.getRelays().contains(author)) {


				toDelete.terminate(state);

			} else throw new BlorkError("Transaction issuer not part of the path");
		}
	}

	/* (non-Javadoc)
	 * @see com.orange.rd.blork.utils.BSONTransaction#getId()
	 */
	@Override
	public int getId() {
		return 13;
	}

}
