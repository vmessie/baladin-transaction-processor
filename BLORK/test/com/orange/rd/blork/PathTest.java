package com.orange.rd.blork;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.AbstractMap;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.PathList;
import com.orange.rd.blork.utils.PublicKey;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

class PathTest {
	private PublicKey key1;
	private PublicKey key2;
	private PublicKey key3;
	private PublicKey key4;
	private PublicKey key5;
	private PublicKey key6;
	private PublicKey key7;
	private PublicKey key8;



	private Path path1;
	private Path path2;
	private Path path3;

	private PathList list1;
	private PathList list2;
	private PathList list3;
	private PathList list4;

	static State mockState;
	private static Map<String, ByteString> mockStateArray;

	@BeforeAll
	static public void setup() {

	}


	@BeforeEach
	public void initPath() {
		mockStateArray = new HashMap<String, ByteString>();
		mockState = Mockito.mock(State.class);

		try {
			Mockito.when(mockState.getState(Mockito.any())).thenAnswer(new Answer<Map<String,ByteString>>() {

				@Override
				public Map<String, ByteString> answer(InvocationOnMock invocation) throws Throwable {
					Collection<String> wantedAddr = null;
					Object arg = invocation.getArgument(0);

					if (!(arg instanceof Collection<?>)) throw new Throwable(arg.toString());

					else  wantedAddr = (Collection<String> ) arg;

					//On simule l'enregistrement de n'importe quel client / relais
					for (String w : wantedAddr) {
						if (w.substring(0, 8).equals(Constants.GLOBAL_NAMESPACE + "01") || w.substring(0, 8).equals(Constants.GLOBAL_NAMESPACE + "02")) {
							mockStateArray.put(w, ByteString.copyFrom(new byte[] {0x01}));
						}
					}
					if (mockStateArray.isEmpty()) throw new InternalError("Empty state");
					return mockStateArray;
				}

			});

			Mockito.when(mockState.setState(Mockito.any())).thenAnswer(new Answer<Collection<String>>() {
				@SuppressWarnings("unchecked")
				@Override
				public Collection<String> answer(InvocationOnMock invocation) throws Throwable {
					Collection<Map.Entry<String, ByteString>> stateMap = null;
					Object arg = invocation.getArgument(0);

					if (!(arg instanceof Collection<?>)) throw new Throwable(arg.toString());

					else  stateMap = (Collection<Map.Entry<String, ByteString>> ) arg;
					Set<String> keySet = new HashSet<String>();

					for (Map.Entry<String, ByteString> k : stateMap) {
						mockStateArray.put(k.getKey(), k.getValue());
						keySet.add(k.getKey());
					}

					return keySet;
				}
			});
		} catch (InternalError | InvalidTransactionException e) {
			fail("Internal error! ");
			e.printStackTrace();
		}
		try {
			key1 = new PublicKey(ByteBuffer.wrap(Base64.getDecoder().decode("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAitIW0wIA9VIc0p2wPkwa" +
					"uUWmMxQhg6gHm2dEakYpcPDqmWMFW6elizXpN2O0oTx+NzTeJ1tsxw2c17emlMFJ" +
					"5hoH9CZnEzhw3T4cZa1s0GD3qzRzvToPFH8ISq4XMQp9pH+c7NKAL9EIaYBp9Lr6" +
					"7BlZbeocYRy+wEF90QsUC2m3picu7WddvY/GPCeCVjgjxbfoov7Mp6qH4m+H3jJj" +
					"duqo0GDQEG6wRMbOmSP18+UdixZmyErd58tghse+3HiuinzytiJ8gLrZblDEcMwk" +
					"f4svDGYgfbnUbn6ke3g9bZH6PEuDm6/JBOoYBO26eNfhfJMAkNQ9PgaFyC76yyzg" +
					"JwIDAQAB")));

			key2 = new PublicKey(ByteBuffer.wrap(Base64.getDecoder().decode("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAggeww/b8OS8/bBTLUO1N" +
					"IKYVmvih0CieXSzLSap7hqI6senVQXkxEjaE3JrndR10VIlB8kUwn65E9iKubnNy" +
					"ZM43u4xHhAvtbr0dQaoLj1pJnr5PkaM/21HyG2mRjAFgDXdPqhjDkdWPhnEvHyET" +
					"JOCEcBrOIdeRd0sqiRFVspHBdOCIrIpqJzlEzGnvk3XvwdIJCSxnU4fpG5BKRU3E" +
					"P14/mD3NB9tGpQ1R5MNy7vIYtmQjw+q7qQOCupBuq89Y5qU8bvy0FcUjea3cCVES" +
					"+x7yQmVve6y5GSHHyt/6BzjijZcTTeKQ+3LvqGQT7QTVqVJLBARtnZJ4xaV5763k" +
					"JQIDAQAB")));
			key3 = new PublicKey(ByteBuffer.wrap(Base64.getDecoder().decode("MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQBUIZYHbdNixRsfPKlXZQF3" +
					"nhvUZoUVwaYtVKil8LfPBO8oNpqbcrAFZ0dtixJwr9MqD4YUG+MayQd0kFa3Dpd3" +
					"idfQPZW2h3a1PasUTqm9mZCRq4tN0n8W8JX9Y8wzsVdUhyi2Xx+q0v86sFS1QXqH" +
					"cIwvEZuKkMBYm+KGZ8kJVgFEzGItJnDhEPQcINEmYl0yD6IW/1O/ODtTRxYN1NlA" +
					"ftnVgerlTQRMFH+dLWzK9ytdzKeIdTh0RpnXvYGN7krjurcCghgAi9j7WRxdoIiA" +
					"KNMBBnrX8BdMjlwVMT1n4EZ/k7FE1lBCGl3zsi6QqhIc0qO41uJc08rXdntuxG+f" +
					"AgMBAAE=")));
			key4 = new PublicKey(ByteBuffer.wrap(Base64.getDecoder().decode("MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQBfbX/Hcq1gR9TZetlWM2Ef" +
					"dkZI8lHwaOshHIqQqJGKeyj1FEbWpPMdPKvpFrv47iWEE5unAupvf38rqjoKV9qS" +
					"fDP0zooNRYblSUGqSs9wWSuZx7cBf7iGpuXKvw+CofR/w+2zQksAfxeA9h8qBtzQ" +
					"tq7iSJRoMNeG7zGd1v5dFnJTWZ2uTEVot/S+u62cUAeyq2RBJijlmogU9tUkMgQO" +
					"8m573xo9M8dNFThxsGQxlHrZN9lY+PqkRDliSO7/mXebH+v6cd2QGQtCC+Qporke" +
					"sQB+Rthjr20CtaBHb0Q2AsN6nnno4u3jku8C0NypzIm9P2BOCGCKRvyq7uqDI2Dz" +
					"AgMBAAE=")));
			key5 = new PublicKey(ByteBuffer.wrap(Base64.getDecoder().decode("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkAGWea4oE2nf09Qdl/O7" +
					"Axj/pmATQMpxvb/elkX7/X3jZ2BTs0iGJwa32m84PMtIH5vJvJCj9iFvJf1TFi4X" +
					"NhIcZ5+yNseAKmCfuz0UQRBudwHAmSyNA0Wii6tT99p/sYQz7ZNELgh6cGg0P3vh" +
					"vSAPr34SilsD8ux6/CfaWXGJ1NEHvApHoHLPKKHULwOkkz5KSVhh5Xy/qBg2AhRG" +
					"XlHk77jgNCVmR54F3tuEqsrDzxbOL+aFxaoabYer7LTuJcGYnqUMlPlgObPanFju" +
					"m/jtj0YKlkvRMuFWoz5UQDeKCCDsTp2FSAm5RcqPwJF8bzofVBTlqwemyPYTht1o" +
					"8wIDAQAB")));
			key6 = new PublicKey(ByteBuffer.wrap(Base64.getDecoder().decode("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjKcB9826kKVH6bO6r+7w" +
					"SI0CzUNyTiBU7vHolh6vnpjMNkbnDvndtxrKYTBDcb/p73i3JpVCfdmzqRt92c/H" +
					"3hnoPq36L8wpV1FexX+xD7/jqvlnpvsNXVEMnrofvPYoejZyqrlrnLWZDEBODxIb" +
					"7QjDlZXDDld/CencBhG8nVf1KrWe3sWe0Mf0LTWidizu2vQo6HLVmIjJGHcntggj" +
					"5+KnAYITID1kYw/XpxEtiW4jTJF9HEp7S5sv1Ea9IY5GiHL/UiTV7Do9Zk2WW313" +
					"DwAj3qDiu1KWODmZIPBitl8HNLgJLwjwU//h0hQLlrzPXF8U4czxkMFLHhK9blu+" +
					"IwIDAQAB")));
			key7 = new PublicKey(ByteBuffer.wrap(Base64.getDecoder().decode("MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQB67zcTaMDkb24V2tlNctWi" +
					"RhDtptV8CMZciXgSEVmhTjlSxDgzpLN12r3gg8hPn2uA0p6SWiuezBzJKR009KbK" +
					"ZUlq05zA2WwWId3IlM2s0g0GAcE6r/CQX15AzhjXYHx1bRi0lHquSEU3rqZsaUNb" +
					"A7+kQwIH0g0MyhD3M1wDTI5Yxn+MzsCzudxCHBQEe9r1zKWpTCvjbVUUUTm32q66" +
					"bQUUNNEDQyqZn2yGrhcA9d+SIqocvPVWpxNC6EGz8S0TladbAvvjTaHKE8Tz81rp" +
					"jAMmKdLZcymSy8fHBvclVB+AZrzwc32JRdrPD8HRTf/P+twIUFHOoigzXiQDTHOb" +
					"AgMBAAE=")));
			key8 = new PublicKey(ByteBuffer.wrap(Base64.getDecoder().decode("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAz9P/nLp+9zNfhNfffNol" +
					"X/LaaeviiEG24lvm2TjKwpV4y8+ED3GktdMwNjuLuKZ25oNcID24/UFBLHXTUcap" +
					"9VHhQEupdYsGCcEpaQGd0NB2vb9yjs7ZQv+1ePuA+Iq9VtI/ZFYXFPLPW0IgcNRc" +
					"GV1EH2eBczmkJ4cnUxM/RTBUn7HJW9OZ4k6a2wKtHyPJhEmfMgqoLIl7Wp5A3GeX" +
					"nNq5ydxx+xjVoYf+CZX8wZZPGhqMN/6PEU0O/HC/b/7+5X3MZLNxZt3kMZpejNDy" +
					"r9MXF9X83xlm8LMfu7ASTERjrCJUOjPibgh7P5TAJQH7qHMQA4oSx48ykDHoeEGX" +
					"3QIDAQAB")));

			list1 = new PathList();
			list1.add(key2);
			list1.add(key3);
			list1.add(key4);
			list1.add(key5);
			list1.add(key6);
			list1.add(key7);

			list2 = new PathList();
			list2.add(key2);
			list2.add(key3);
			list2.add(key4);


			list3 = new PathList();
			list3.add(key2);
			list3.add(key4);
			list3.add(key3);

			list4 = new PathList();
			list4.add(key1);
			list4.add(key2);
			list4.add(key4);
			list4.add(key5);
			list4.add(key6);
			list4.add(key7);

			path1 = new Path(key1, key8, list1, mockState);
			path2 = new Path(key1, key5, list2, mockState);



		} catch (InvalidKeySpecException e) {
			fail("Internal error ! " + e.getMessage());
			e.printStackTrace();
		} catch (BlorkError e) {
			fail("Error while creating path : " + e.getMessage());
		}
	}

	private void initPhase2() throws BlorkError {
		path1.terminate(mockState);
		path3 = new Path(key1, key8, list1, mockState);
	}


	@Test
	void testPath() throws InvalidKeySpecException, BlorkError {

		Executable duplicateKey = () -> {
			new Path(key1, key8, list4, mockState);
		};

		Executable duplicatePath = () -> {
			new Path(key1, key8, list1, mockState);
		};

		//TODO test de la non-authentification des utilisateurs

		assertThrows(BlorkError.class, duplicateKey,  "Path with redundant keys should not be created");
		assertThrows(BlorkError.class, duplicatePath, "Path that override an existing path should not be created");


		initPhase2();
	}

	@Test
	void testGetId() throws InvalidKeySpecException, BlorkError {
		initPhase2();

		ByteBuffer exceptedIdFor2 = ByteBuffer.wrap(Base64.getDecoder().decode("oUd1u42UWMrbpn+C48HxWQ=="));

		assertArrayEquals(exceptedIdFor2.array(), path2.getId().array(), "Invalid ID");
		assertArrayEquals(path1.getId().array(), path3.getId().array(), "id of same path should be equal");
	}

	@Test
	void testTerminate() throws InvalidKeySpecException, BlorkError {

		assertEquals(true, path1.isActive(), "Path should be active");

		path1.terminate(mockState);

		assertEquals(false, path1.isActive(), "Path should be inactive");
	}

	@Test
	void testPutData() throws InvalidKeySpecException, BlorkError {

		Random r = new Random();

		Executable notGoodFrame1 = () -> path1.putData(42, 42, 64, mockState);
		Executable notGoodFrame2 = () -> {
			path1.putData(42, 1, 0xFFF, mockState);
			path1.putData(42, 2, 0xDDD, mockState);
		};

		assertThrows(BlorkError.class, notGoodFrame1, "Wrong frame number should throw an error");
		assertThrows(BlorkError.class, notGoodFrame2, "Wrong frame timestamp should throw an error");

		long excepted = 42;

		int iteration = r.nextInt(20);

		for (int i = 0; i < iteration; i++) {
			long inc = r.nextLong();
			excepted += inc;

			path1.putData(inc, i+2, 64*i + 0xFFF, mockState);
		}

		assertEquals(excepted, path1.getDataExchanged(), "Probem while storing data");


		path1.terminate(mockState);

		Executable e = () -> path1.putData(r.nextLong(), iteration + 1, 64, mockState);

		assertThrows(BlorkError.class, e, "This should throw an error");

		notGoodFrame1 = () -> path1.putData(42, 1, 97, mockState);

		assertThrows(BlorkError.class, notGoodFrame1, "Wrong frame number should throw an error");
	}

	@Test
	void testIterator() throws InvalidKeySpecException, BlorkError {
		initPath();
		List<PublicKey> forTest = new LinkedList<PublicKey>();

		forTest.add(key1);
		forTest.add(key2);
		forTest.add(key3);
		forTest.add(key4);
		forTest.add(key5);

		int i = 0;

		for (PublicKey k : path2) {
			assertArrayEquals(k.getInstance().getEncoded(), forTest.get(i).getInstance().getEncoded(), "Keys should be equals when iterating");
			i++;
		}
	}

	@Test
	void testGetPathById() throws InvalidKeySpecException, BlorkError, InternalError, InvalidTransactionException {
		Random r = new Random();

		path2.putData(r.nextLong(), 1, 64, mockState);

		ByteBuffer exceptedIdFor2 = ByteBuffer.wrap(Base64.getDecoder().decode("oUd1u42UWMrbpn+C48HxWQ=="));

		Path excepted2 = Path.getPathById(ByteBuffer.wrap(exceptedIdFor2.array()), mockState);
		assertArrayEquals(path2.getId().array(), excepted2.getId().array(), "Ids should be the same");

		assertEquals(path2.getDataExchanged(), excepted2.getDataExchanged(), "Exchanged data should be equal");

		ByteBuffer randomId = ByteBuffer.allocate(16).putLong(r.nextLong()).putLong(r.nextLong());

		assertNull(Path.getPathById(randomId, mockState), "For a non existing ID, the method should return null");

	}

	@Test
	void testEquals() throws InvalidKeySpecException, BlorkError {
		initPath();
		initPhase2();

		assertEquals(false, path1.equals(new Object()), "this should not be equal");
		assertEquals(false, path1.equals(path2), "path are not equals");

		assertEquals(true, path1.equals(path3), "Path should be equals");
		assertEquals(true, path3.equals(path1), "Path should be equals");
		assertEquals(true, path1.equals(path1), "Path is equal to itself");
		Path path7 = path1;
		assertEquals(true, path1.equals(path7), "Path should be equals");
	}

	@Test
	void testGetLastFrame() throws BlorkError {
		path1.putData(0xFED, 1, 0x42, mockState);
		path1.putData(0x45f, 2, 0x654, mockState);

		assertEquals(2, path1.getLastFrame());
	}

	@Test
	void testGetDataExchanged() throws BlorkError {
		path1.putData(0x456, 1, 545, mockState);
		path1.putData(0x789, 2, 546, mockState);

		assertEquals(0x456 + 0x789, path1.getDataExchanged());
	}
}
