/**
 *
 */
package com.orange.rd.blork;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bson.BsonArray;
import org.bson.BsonBinary;
import org.bson.BsonBinaryReader;
import org.bson.BsonBinaryWriter;
import org.bson.BsonReader;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.io.BasicOutputBuffer;
import org.bson.io.OutputBuffer;
import org.bson.types.Binary;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

/**
 * @author lsmv2780
 *
 */

class DelPathTransactionTest {
	final String PATH_PREFIX = "/tmp/py_pou_test/";


	private Map<String, ByteString> mockStateArray;
	private State mockState;

	private Document delPathReqEmitter;
	private Document delPathReqRelay;
	private Document delPathReqReceiver;

	static private DelPathTransaction tx;


	private ByteBuffer file2ByteBuffer(String path) throws IOException {

		InputStream stream 	= new FileInputStream(path);

		int byteRead;
		byte[] buffer = new byte[10000];


		int position = 0;

		while ((byteRead  = stream.read()) != -1) {
			buffer[position] = (byte) byteRead;
			position++;
		}

		stream.close();

		return ByteBuffer.wrap(Arrays.copyOfRange(buffer, 0 ,position));
	}


	@BeforeAll
	static void init() {
		tx = new DelPathTransaction();
	}

	@BeforeEach
	void setup() {
		mockStateArray = new HashMap<String, ByteString>();

		//Création d'un state associé à un path déjà créé
		ByteBuffer headers = null;

		//exécution du script de génération
		try {
			Process p = Runtime.getRuntime().exec("../py_gen_pou/main_bson.py");
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			fail("Cannot generate PoUs");
		}

		try {
			headers = file2ByteBuffer(PATH_PREFIX + "pou_1_headers");


			headers.position(12);
			headers.limit(12 + 16);

			ByteBuffer pathId = ByteBuffer.allocate(16);
			pathId.put(headers);

			headers.position(12);
			headers.limit(12 + 16);

			String id = Constants.GLOBAL_NAMESPACE + "03" + BlorkUtils.byteToHex(headers) + "ffffffffffffffffffffffffffffff";

			Document bsonBody = new Document();

			bsonBody.put("emitter_key",  new Binary(file2ByteBuffer(PATH_PREFIX + "key_emitter").array()));
			bsonBody.put("receiver_key", new Binary(file2ByteBuffer(PATH_PREFIX + "key_receiver").array()));

			BsonArray rlys = new BsonArray();
			rlys.add(new BsonBinary(file2ByteBuffer(PATH_PREFIX + "key_relay1").array()));
			rlys.add(new BsonBinary(file2ByteBuffer(PATH_PREFIX + "key_relay2").array()));
			rlys.add(new BsonBinary(file2ByteBuffer(PATH_PREFIX + "key_relay3").array()));
			rlys.add(new BsonBinary(file2ByteBuffer(PATH_PREFIX + "key_relay4").array()));

			bsonBody.put("relay_keys", rlys);
			bsonBody.put("status", true);
			bsonBody.put("data_amount", (long) 0);
			bsonBody.put("last_pou", (int) 0);
			bsonBody.put("last_timestamp", (long) 0);

			DocumentCodec codec 	= new DocumentCodec();

			OutputBuffer output = new BasicOutputBuffer();
			BsonBinaryWriter writer = new BsonBinaryWriter(output);


			codec.encode(writer, bsonBody, EncoderContext.builder().build());


			ByteBuffer bson = ByteBuffer.wrap(output.toByteArray());

			ByteString body = ByteString.copyFrom(bson);

			mockStateArray.put(id, body);

			mockState = Mockito.mock(State.class);

			try {
				Mockito.when(mockState.getState(Mockito.any())).thenReturn(mockStateArray);
				Mockito.when(mockState.setState(Mockito.any())).thenAnswer(new Answer<Collection<String>>() {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<String> answer(InvocationOnMock invocation) throws Throwable {
						Collection<Map.Entry<String, ByteString>> stateMap = null;
						Object arg = invocation.getArgument(0);

						if (!(arg instanceof Collection<?>)) throw new Throwable(arg.toString());

						else  stateMap = (Collection<Map.Entry<String, ByteString>> ) arg;
						Set<String> keySet = new HashSet<String>();

						for (Map.Entry<String, ByteString> k : stateMap) {
							mockStateArray.put(k.getKey(), k.getValue());
							keySet.add(k.getKey());
						}

						return keySet;
					}
				});
			} catch (InternalError | InvalidTransactionException e) {
				fail("Internal error! ");
				e.printStackTrace();
			}

			//Décodage BSON

			delPathReqEmitter = new Document();

			delPathReqEmitter.put("path_id",    new Binary(pathId.array()));
			delPathReqEmitter.put("author_key", new Binary(file2ByteBuffer(PATH_PREFIX + "key_emitter").array()));
			delPathReqEmitter.put("author_sig", new Binary(file2ByteBuffer(PATH_PREFIX + "del_sig_emitter").array()));

			delPathReqRelay = new Document();

			delPathReqRelay.put("path_id", 	  new Binary(pathId.array()));
			delPathReqRelay.put("author_key", new Binary(file2ByteBuffer(PATH_PREFIX + "key_relay3").array()));
			delPathReqRelay.put("author_sig", new Binary(file2ByteBuffer(PATH_PREFIX + "del_sig_relay3").array()));

			delPathReqReceiver = new Document();

			delPathReqReceiver.put("path_id",    new Binary(pathId.array()));
			delPathReqReceiver.put("author_key", new Binary(file2ByteBuffer(PATH_PREFIX + "key_receiver").array()));
			delPathReqReceiver.put("author_sig", new Binary(file2ByteBuffer(PATH_PREFIX + "del_sig_receiver").array()));


		} catch (IOException e1) {
			fail("Unable to open file : " + e1.getMessage());
		}
	}
	/**
	 * Test method for {@link com.orange.rd.blork.DelPathTransaction#hydrate(org.bson.Document)}.
	 */
	@Test
	void testHydrate() {
		Document delPathReqRelayClone = new Document(delPathReqRelay);

		Executable invalidDoc1 = () -> {
			Document notGood = new Document();

			notGood.put("edze", "éd");
			notGood.put("azdaffsdf", 5415);
			tx.hydrate(notGood);
		};

		Executable invalidDoc2 = () -> {
			delPathReqRelayClone.remove("author_key");
			tx.hydrate(delPathReqRelayClone);
		};

		Executable invalidKeyType = () -> {
			delPathReqRelayClone.put("author_sig", "not good lol");
			tx.hydrate(delPathReqRelayClone);
		};

		assertThrows(DecodeError.class, invalidDoc1);
		assertThrows(DecodeError.class, invalidDoc2);
		assertThrows(DecodeError.class, invalidKeyType);


		try {
			tx.hydrate(delPathReqEmitter);
		} catch (DecodeError e) {
			fail("Error: " + e.getMessage());
		}
	}

	/**
	 * Test method for {@link com.orange.rd.blork.DelPathTransaction#toBSON()}.
	 */
	@Test
	void testToBSON() {
		try {
			tx.hydrate(delPathReqEmitter);
		} catch (DecodeError e) {
			fail("Error: " + e.getMessage());
		}
		assertEquals(delPathReqEmitter.toJson(), tx.toBSON().toJson());
	}

	/**
	 * Test method for {@link com.orange.rd.blork.DelPathTransaction#process(sawtooth.sdk.processor.State)}.
	 * @throws DecodeError
	 * @throws BlorkError
	 */
	@Test
	void testProcessEmitter() throws DecodeError, BlorkError {
		tx.hydrate(delPathReqEmitter);
		tx.process(mockState);
	}

	/**
	 * Test method for {@link com.orange.rd.blork.DelPathTransaction#process(sawtooth.sdk.processor.State)}.
	 * @throws DecodeError
	 * @throws BlorkError
	 */
	@Test
	void testProcessRelay() throws DecodeError, BlorkError {
		tx.hydrate(delPathReqRelay);
		tx.process(mockState);
	}

	/**
	 * Test method for {@link com.orange.rd.blork.DelPathTransaction#process(sawtooth.sdk.processor.State)}.
	 * @throws DecodeError
	 * @throws BlorkError
	 */
	@Test
	void testProcessReceiver() throws DecodeError, BlorkError {
		tx.hydrate(delPathReqReceiver);
		tx.process(mockState);
	}

	/**
	 * Test method for {@link com.orange.rd.blork.DelPathTransaction#process(sawtooth.sdk.processor.State)}.
	 * @throws DecodeError
	 */
	@Test
	void testProcessSignature() throws DecodeError {
		delPathReqRelay.put("author_sig", new Binary(new byte[] {0x00, 0x42, 0x56}));
		tx.hydrate(delPathReqRelay);

		Executable shouldFail = () -> tx.process(mockState);

		assertThrows(BlorkError.class, shouldFail);
	}

}
