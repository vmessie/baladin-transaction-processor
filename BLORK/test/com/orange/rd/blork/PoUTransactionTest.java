/**
 *
 */
package com.orange.rd.blork;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bson.BsonArray;
import org.bson.BsonBinary;
import org.bson.BsonBinaryReader;
import org.bson.BsonBinaryWriter;
import org.bson.BsonReader;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.io.BasicOutputBuffer;
import org.bson.io.OutputBuffer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

/**
 * @author lsmv2780
 *
 */
class PoUTransactionTest {

	final String PATH_PREFIX = "/tmp/py_pou_test/";


	private Map<String, ByteString> mockStateArray;
	private State mockState;


	private static PoUTransaction tx;

	private Document pou1;
	private Document pou2;
	private Document pou3;

	private ByteBuffer file2ByteBuffer(String path) throws IOException {

		InputStream stream 	= new FileInputStream(path);

		int byteRead;
		byte[] buffer = new byte[10000];


		int position = 0;

		while ((byteRead  = stream.read()) != -1) {
			buffer[position] = (byte) byteRead;
			position++;
		}

		stream.close();

		return ByteBuffer.wrap(Arrays.copyOfRange(buffer, 0 ,position));
	}

	@BeforeAll
	static void init() {
		tx = new PoUTransaction();
	}

	@BeforeEach
	void setup() {
		mockStateArray = new HashMap<String, ByteString>();

		//Création d'un state associé à un path déjà créé
		ByteBuffer headers = null;

		//exécution du script de génération
		try {
			Process p = Runtime.getRuntime().exec("../py_gen_pou/main_bson.py");
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			fail("Cannot generate PoUs");
		}

		try {
			headers = file2ByteBuffer(PATH_PREFIX + "pou_1_headers");


			headers.position(12);
			headers.limit(12 + 16);

			String id = Constants.GLOBAL_NAMESPACE + "03" + BlorkUtils.byteToHex(headers) + "ffffffffffffffffffffffffffffff";

			Document bsonBody = new Document();

			bsonBody.put("emitter_key",  file2ByteBuffer(PATH_PREFIX + "key_emitter").array());
			bsonBody.put("receiver_key", file2ByteBuffer(PATH_PREFIX + "key_receiver").array());

			BsonArray rlys = new BsonArray();
			rlys.add(new BsonBinary(file2ByteBuffer(PATH_PREFIX + "key_relay1").array()));
			rlys.add(new BsonBinary(file2ByteBuffer(PATH_PREFIX + "key_relay2").array()));
			rlys.add(new BsonBinary(file2ByteBuffer(PATH_PREFIX + "key_relay3").array()));
			rlys.add(new BsonBinary(file2ByteBuffer(PATH_PREFIX + "key_relay4").array()));

			bsonBody.put("relay_keys", rlys);
			bsonBody.put("status", true);
			bsonBody.put("data_amount", (long) 0);
			bsonBody.put("last_pou", (int) 0);
			bsonBody.put("last_timestamp", (long) 0);

			DocumentCodec codec 	= new DocumentCodec();

			OutputBuffer output = new BasicOutputBuffer();
			BsonBinaryWriter writer = new BsonBinaryWriter(output);


			codec.encode(writer, bsonBody, EncoderContext.builder().build());


			ByteBuffer bson = ByteBuffer.wrap(output.toByteArray());

			ByteString body = ByteString.copyFrom(bson);

			mockStateArray.put(id, body);

			mockState = Mockito.mock(State.class);

			try {
				Mockito.when(mockState.getState(Mockito.any())).thenReturn(mockStateArray);
				Mockito.when(mockState.setState(Mockito.any())).thenAnswer(new Answer<Collection<String>>() {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<String> answer(InvocationOnMock invocation) throws Throwable {
						Collection<Map.Entry<String, ByteString>> stateMap = null;
						Object arg = invocation.getArgument(0);

						if (!(arg instanceof Collection<?>)) throw new Throwable(arg.toString());

						else  stateMap = (Collection<Map.Entry<String, ByteString>> ) arg;
						Set<String> keySet = new HashSet<String>();

						for (Map.Entry<String, ByteString> k : stateMap) {
							mockStateArray.put(k.getKey(), k.getValue());
							keySet.add(k.getKey());
						}

						return keySet;
					}
				});
			} catch (InternalError | InvalidTransactionException e) {
				fail("Internal error! ");
				e.printStackTrace();
			}

			//Décodage BSON
			ByteBuffer pou1bin = file2ByteBuffer(PATH_PREFIX + "bson_1");
			ByteBuffer pou2bin = file2ByteBuffer(PATH_PREFIX + "bson_2");
			ByteBuffer pou3bin = file2ByteBuffer(PATH_PREFIX + "bson_3");

			BsonReader reader1 = new BsonBinaryReader(pou1bin);
			BsonReader reader2 = new BsonBinaryReader(pou2bin);
			BsonReader reader3 = new BsonBinaryReader(pou3bin);

			pou1 = codec.decode(reader1, DecoderContext.builder().build());
			pou2 = codec.decode(reader2, DecoderContext.builder().build());
			pou3 = codec.decode(reader3, DecoderContext.builder().build());

		} catch (IOException e1) {
			fail("Unable to open file : " + e1.getMessage());
		}
	}
	/**
	 * Test method for {@link com.orange.rd.blork.PoUTransaction#hydrate(org.bson.Document)}.
	 */
	@Test
	void testHydrate() {
		Executable invalidDoc1 = () -> {
			Document notGood = new Document();

			notGood.put("edze", "éd");
			notGood.put("azdaffsdf", 5415);
			tx.hydrate(notGood);
		};

		Executable invalidDoc2 = () -> {
			pou1.remove("emitter_key");
			tx.hydrate(pou1);
		};

		Executable invalidKeyType = () -> {
			pou1.put("emitter_key", "not good lol");
			tx.hydrate(pou1);
		};

		assertThrows(DecodeError.class, invalidDoc1);
		assertThrows(DecodeError.class, invalidDoc2);
		assertThrows(DecodeError.class, invalidKeyType);


		try {
			tx.hydrate(pou2);
		} catch (DecodeError e) {
			fail("Error: " + e.getMessage());
		}
	}

	/**
	 * Test method for {@link com.orange.rd.blork.PoUTransaction#toBSON()}.
	 * @throws DecodeError
	 */
	@Test
	void testToBSON() throws DecodeError {
		tx.hydrate(pou1);
		assertEquals(pou1.toJson(), tx.toBSON().toJson());
	}

	/**
	 * Test method for {@link com.orange.rd.blork.PoUTransaction#process(sawtooth.sdk.processor.State)}.
	 * @throws BlorkError
	 */
	@Test
	void testProcess() throws BlorkError {
		try {
			tx.hydrate(pou1);
			tx.process(mockState);
			tx.hydrate(pou2);
			tx.process(mockState);
			tx.hydrate(pou3);
			tx.process(mockState);
		} catch (DecodeError e) {
			fail("Error : " + e.getMessage());
		}
	}

}
