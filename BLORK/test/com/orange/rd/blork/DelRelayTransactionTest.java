/**
 *
 */
package com.orange.rd.blork;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.bson.Document;
import org.bson.types.Binary;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

/**
 * @author lsmv2780
 *
 */
class DelRelayTransactionTest {

	static DelRelayTransaction tx;
	Document data;
	@BeforeAll
	static void setup() {
		tx 		= new DelRelayTransaction();
	}

	@BeforeEach
	void init() {
		data	= new Document();
		KeyPairGenerator keyGen;
		try {
			keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(2048);

			KeyPair keys = keyGen.genKeyPair();

			data.put("relay_key", new Binary(keys.getPublic().getEncoded()));

			Signature signer = Signature.getInstance("SHA256withRSA");


			KeyFactory kf = KeyFactory.getInstance("RSA");
			PrivateKey pk = kf.generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode("MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCCSMnvxtwh3RZW" +
					"ny0FK5VuJ07W/ntKjho0JsENp59JjzBrCZRih1dgbdpdaGNgFrbqKgdwQGIt/9wU" +
					"nfnrSsaPFTPzeDfoemqjkV4HOpBmhMu6sWrIcWsTu+CBuchgnL8kNHAbLbSYFrp0" +
					"OnYF41EgKL47GkcnB+/UpjrmwfqnyAdxU41YrRCGGxNS9UUfG5p/R4llOa1XJZZx" +
					"xihL+DAMqYYNwNp+EneyHB2oGoes8b+0akiNGVLmjkMGFxsRrWV8smiFJ5AqnBQ6" +
					"nvSp105pqdVVYpg5B3yE7Yflpb8u8rNVjFiGgiZsjsyFaO4OGgRoEeOI2+Ep8PDO" +
					"rkfHJd1FAgMBAAECggEABGtRpjd5mhB0xIVrsoQy8mSU8Qf6yKGfU+uImtz3szgP" +
					"LWJKzTjIBvZS9N4omMbMd47rZ6xXaDYIkg51vrxvlcTjWTvXsaQNX/T7WoVrBn80" +
					"peWEuL3YIvg5ek0B9fz7ItBAiTt4p7wFoRGmlTaLtuqK4VhX14QWZ4En+FHWHcDH" +
					"1BFrYExrbwnzADTJ0W4syUpHjNf0djDMOFkl5FlF6WBIVcT1l0cSuFhrtjooffHV" +
					"NytnS37e7BdJAK/KZW0ENYuX7XD1ZLFORdLNf5op+pSD49W6UaP9pLKNdmJf0lYF" +
					"Opb0XhZv7K26VzkN0gmX8u5niMNJvchUaZI295uNwQKBgQC3tXPM3/XkLDmvxs0V" +
					"yThT0CUesKAOU8zvugo4yyvdzgYX8f66FSvAaJ/ouxVSjJJWF+bNUP8U8prkCwuC" +
					"n2f5xzt+vDTjlP6TWybMNRpaC+4FWSBpx4QdbgQ6aFXJto9AMSjrG5ZlShK0Dyc+" +
					"/cIDQASMZH2DsqeJa9RU91KpNQKBgQC1jW+melnsuup7Xh4MTUBqVH8GCq5uDa5I" +
					"Ypa2GKy8zsvE7s/nflwQX1lsA0LZrYVllJyIX+XpUKFd1RD/msKGtOlT19ERCzpa" +
					"2vgttOvB9KavH+MKs5REaIl4roEbvJTJyYGaq6nEOw3FNQfeYkov2dFnXD3U/lxh" +
					"/Cv+zxL10QKBgCRgULLDshvhSfbtZZSnyHiIIaGHFdpga5cYfoeqa2A7OGeramYM" +
					"ZSJvVSKvUNSMszlK+E+1dxz9wP/g1AGkcOE7uyMoP0zFJvekt/T9mt8zt4jlkg6A" +
					"DEALldi+6iXL+WeblF+hkdEyrqtqyLmnY7BjD8OA7n2Sdaw3Zs8APiT1AoGBAIuN" +
					"lG0bT1QwuVzrE53RI9qX6Kv0OfBOg9EQN1jxzpWzP9640wbWkl3jbREh0JkSUJ+s" +
					"hEYvsMKPP/qodNCFXVG+bjwMgJ/hSZHdDzfTAgWs0RN8J0FqmGWdFU62lHeFlbkm" +
					"M4F4wp4b5pHYGZxPYUc230fcF4tJmimXdRoAgkqBAoGAVn2pnjbJr1wNFXxml/PT" +
					"yiZuxhZqww/t28NzNb0gUShcx6zQ/CZD/9bxFjw2J86PADjt1ex+U35IYX+SjsfE" +
					"rfzDUjPxGg9dAy31ODDDeu1Evho2QvvrKl5yiySTLjr5TVNTaCROCJfab9g64tZC" +
					"pwu3Vd4NnVx9dyPWC9A9izA=")));


			signer.initSign(pk);

			signer.update(keys.getPublic().getEncoded());

			data.put("issuer_sig", new Binary(signer.sign()));
		} catch(SignatureException | InvalidKeySpecException | InvalidKeyException | NoSuchAlgorithmException e) {
			fail("Internal error : " + e.getMessage());
		}

		try {
			tx.hydrate(data);
		} catch (DecodeError e) {
			fail("La transaction a échouée : " + e.getMessage());
		}
	}


	/**
	 * Test method for {@link com.orange.rd.blork.DelRelayTransaction#hydrate(org.bson.Document)}.
	 */
	@Test
	void testHydrate() {
		Executable malformedDocument = () -> {
			Document invalid = new Document();
			invalid.put("zeg", 561.654);
			invalid.put("zzg", "ER");

			tx.hydrate(invalid);
		};

		Executable malformedDocument2 = () -> {
			data.put("relay_key", 42);
			tx.hydrate(data);
		};

		Executable malformedDocument3= () -> {
			data.put("issuer_sig", 0.42);
			tx.hydrate(data);
		};

		Executable invalidRelayKey = () -> {
			data.put("relay_key", new Binary(new byte[] {0x42}));
			tx.hydrate(data);
		};


		assertThrows(DecodeError.class, malformedDocument);
		assertThrows(DecodeError.class, malformedDocument2);
		assertThrows(DecodeError.class, malformedDocument3);

		assertThrows(DecodeError.class, invalidRelayKey);
	}

	/**
	 * Test method for {@link com.orange.rd.blork.DelRelayTransaction#toBSON()}.
	 */
	@Test
	void testToBSON() {
		assertEquals(data.toJson(), tx.toBSON().toJson());
	}

	/**
	 * Test method for {@link com.orange.rd.blork.DelRelayTransaction#process(sawtooth.sdk.processor.State)}.
	 * @throws DecodeError
	 * @throws BlorkError
	 */
	@Test
	void testProcess() throws InternalError, InvalidTransactionException, DecodeError, BlorkError{
		String exceptedAddr = Constants.GLOBAL_NAMESPACE + "02" + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(((Binary) data.get("relay_key")).getData(), ((Binary) data.get("relay_key")).getData().length - 31, ((Binary) data.get("relay_key")).getData().length)));

		Map<String, ByteString> toReturn = new HashMap<String, ByteString>();
		toReturn.put(exceptedAddr, ByteString.copyFrom(new byte[] {0x01}));


		State fakeStateOneBefore = Mockito.mock(State.class);

		Mockito.when(fakeStateOneBefore.getState(Collections.singleton(exceptedAddr))).thenReturn(toReturn);
		tx.process(fakeStateOneBefore);

		Mockito.verify(fakeStateOneBefore).setState(Collections.singleton(new AbstractMap.SimpleEntry<String, ByteString>(exceptedAddr, ByteString.copyFrom(new byte[] {0x00}))));

		//Relay not registered
		final State fakeStateZeroBefore = Mockito.mock(State.class);

		Executable shouldFail = () -> {
			toReturn.put(exceptedAddr, ByteString.EMPTY);
			Mockito.when(fakeStateZeroBefore.getState(Collections.singleton(exceptedAddr))).thenReturn(toReturn);
			tx.process(fakeStateZeroBefore);
		};


		assertThrows(BlorkError.class, shouldFail);
		Mockito.verify(fakeStateZeroBefore, Mockito.never()).setState(Collections.singleton(new AbstractMap.SimpleEntry<String, ByteString>(exceptedAddr, ByteString.copyFrom(new byte[] {0x00}))));



		//OP signature invalid

		final State fakeStateOneBefore2 = Mockito.mock(State.class);

		Executable shouldFail2 = () -> {
			toReturn.put(exceptedAddr, ByteString.copyFrom(new byte[] {0x01}));
			Mockito.when(fakeStateOneBefore2.getState(Collections.singleton(exceptedAddr))).thenReturn(toReturn);

			data.put("issuer_sig", new Binary(Base64.getDecoder().decode("dnEVLe8pY6A0CaKllTftr0Ku//uPkKxMviEB2BVSG1JY4hxrODRHM0juK0C3amsQ2jAP0K4iIxsg" +
					"+qnACAjQplrR+IIim0NZaFGVv9R3OI24PNSmhbJKeUbsI7E3U9aosIy3meaSauEEna3xoEfnjNLx" +
					"p9JHrff7bsYu/LU2bDyuqla4PyZn+6+U6aYOS4BVV5ekLJwxVsBRMPqIxqP8eqRXt0IBhawasiLs" +
					"J1HiVFjdrQPMX9im3EioOnOVAR+36c7PnWIm/DGyqGAOdh0xyy0PoJgnypSI1AQGCrjW8VBROcVj" +
					"MAY7jPeWvaZYIqkiQ6l3cIRMH80pQ0b9JSoSHg==")));


			tx.hydrate(data);
			tx.process(fakeStateOneBefore2);
		};

		assertThrows(BlorkError.class, shouldFail2);
		Mockito.verify(fakeStateOneBefore2, Mockito.never()).setState(Collections.singleton(new AbstractMap.SimpleEntry<String, ByteString>(exceptedAddr, ByteString.copyFrom(new byte[] {0x00}))));
	}

}
