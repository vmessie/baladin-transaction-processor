/**
 *
 */
package com.orange.rd.blork;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bson.BsonBinaryReader;
import org.bson.BsonReader;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.DocumentCodec;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

/**
 * @author lsmv2780
 *
 */
class NewPathTransactionTest {
	final String PATH_PREFIX = "/tmp/py_pou_test/";


	private Map<String, ByteString> mockStateArray;
	private State mockState;


	private static NewPathTransaction tx;

	private Document pou0;

	private ByteBuffer file2ByteBuffer(String path) throws IOException {

		InputStream stream 	= new FileInputStream(path);

		int byteRead;
		byte[] buffer = new byte[10000];


		int position = 0;

		while ((byteRead  = stream.read()) != -1) {
			buffer[position] = (byte) byteRead;
			position++;
		}

		stream.close();

		return ByteBuffer.wrap(Arrays.copyOfRange(buffer, 0 ,position));
	}

	@BeforeAll
	static void init() {
		tx = new NewPathTransaction();
	}

	@BeforeEach
	void setup() {
		mockStateArray = new HashMap<String, ByteString>();

		//exécution du script de génération
		try {
			Process p = Runtime.getRuntime().exec("../py_gen_pou/main_bson.py");
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			fail("Cannot generate PoUs");
		}

		try {
			mockState = Mockito.mock(State.class);

			ByteBuffer kEmitter   = file2ByteBuffer(PATH_PREFIX + "key_emitter");
			ByteBuffer KReceiver  = file2ByteBuffer(PATH_PREFIX + "key_receiver");
			ByteBuffer kRelay1	  = file2ByteBuffer(PATH_PREFIX + "key_relay1");
			ByteBuffer kRelay2    = file2ByteBuffer(PATH_PREFIX + "key_relay2");
			ByteBuffer kRelay3	  = file2ByteBuffer(PATH_PREFIX + "key_relay3");
			ByteBuffer kRelay4	  = file2ByteBuffer(PATH_PREFIX + "key_relay4");


			kEmitter.position(kEmitter.capacity() - 31);
			KReceiver.position(KReceiver.capacity() - 31);

			kRelay1.position(kRelay1.capacity() - 31);
			kRelay2.position(kRelay2.capacity() - 31);
			kRelay3.position(kRelay3.capacity() - 31);
			kRelay4.position(kRelay4.capacity() - 31);

			String addrForEmitter  = Constants.GLOBAL_NAMESPACE + "01" + BlorkUtils.byteToHex(kEmitter);

			String addrForReceiver = Constants.GLOBAL_NAMESPACE + "01" + BlorkUtils.byteToHex(KReceiver);


			String addrForRly1 = Constants.GLOBAL_NAMESPACE + "02" + BlorkUtils.byteToHex(kRelay1);
			String addrForRly2 = Constants.GLOBAL_NAMESPACE + "02" + BlorkUtils.byteToHex(kRelay2);
			String addrForRly3 = Constants.GLOBAL_NAMESPACE + "02" + BlorkUtils.byteToHex(kRelay3);
			String addrForRly4 = Constants.GLOBAL_NAMESPACE + "02" + BlorkUtils.byteToHex(kRelay4);



			mockStateArray.put(addrForEmitter,  ByteString.copyFrom(new byte[] {0x01}));
			mockStateArray.put(addrForReceiver, ByteString.copyFrom(new byte[] {0x01}));


			mockStateArray.put(addrForRly1, ByteString.copyFrom(new byte[] {0x01}));
			mockStateArray.put(addrForRly2, ByteString.copyFrom(new byte[] {0x01}));
			mockStateArray.put(addrForRly3, ByteString.copyFrom(new byte[] {0x01}));
			mockStateArray.put(addrForRly4, ByteString.copyFrom(new byte[] {0x01}));


			DocumentCodec codec 	= new DocumentCodec();

			try {
				Mockito.when(mockState.getState(Mockito.any())).thenReturn(mockStateArray);
				Mockito.when(mockState.setState(Mockito.any())).thenAnswer(new Answer<Collection<String>>() {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<String> answer(InvocationOnMock invocation) throws Throwable {
						Collection<Map.Entry<String, ByteString>> stateMap = null;
						Object arg = invocation.getArgument(0);

						if (!(arg instanceof Collection<?>)) throw new Throwable(arg.toString());

						else  stateMap = (Collection<Map.Entry<String, ByteString>> ) arg;
						Set<String> keySet = new HashSet<String>();

						for (Map.Entry<String, ByteString> k : stateMap) {
							mockStateArray.put(k.getKey(), k.getValue());
							keySet.add(k.getKey());
						}

						return keySet;
					}
				});
			} catch (InternalError | InvalidTransactionException e) {
				fail("Internal error! ");
				e.printStackTrace();
			}

			//Décodage BSON
			ByteBuffer pou0bin = file2ByteBuffer(PATH_PREFIX + "bson_0");

			BsonReader reader0 = new BsonBinaryReader(pou0bin);

			pou0 = codec.decode(reader0, DecoderContext.builder().build());

		} catch (IOException e1) {
			fail("Unable to open file : " + e1.getMessage());
		}
	}
	/**
	 * Test method for {@link com.orange.rd.blork.NewPathTransaction#hydrate(org.bson.Document)}.
	 */
	@Test
	void testHydrate() {

		Document pou0Clone = new Document(pou0);

		Executable invalidDoc1 = () -> {
			Document notGood = new Document();

			notGood.put("edze", "éd");
			notGood.put("azdaffsdf", 5415);
			tx.hydrate(notGood);
		};

		Executable invalidDoc2 = () -> {
			pou0Clone.remove("emitter_key");
			tx.hydrate(pou0Clone);
		};

		Executable invalidKeyType = () -> {
			pou0Clone.put("emitter_key", "not good lol");
			tx.hydrate(pou0Clone);
		};

		assertThrows(DecodeError.class, invalidDoc1);
		assertThrows(DecodeError.class, invalidDoc2);
		assertThrows(DecodeError.class, invalidKeyType);


		try {
			tx.hydrate(pou0);
		} catch (DecodeError e) {
			fail("Error: " + e.getMessage());
		}
	}

	/**
	 * Test method for {@link com.orange.rd.blork.NewPathTransaction#toBSON()}.
	 */
	@Test
	void testToBSON() {
		try {
			tx.hydrate(pou0);
		} catch (DecodeError e) {
			fail("An error has been thrown ! " + e.getMessage());
		}
		assertEquals(pou0.toJson(), tx.toBSON().toJson());
	}

	/**
	 * Test method for {@link com.orange.rd.blork.NewPathTransaction#process(sawtooth.sdk.processor.State)}.
	 * @throws BlorkError
	 */
	@Test
	void testProcess() throws BlorkError {
		try {
			tx.hydrate(pou0);
			tx.process(mockState);

			Executable notRegistered =() -> {

				ByteBuffer kEmitter   = file2ByteBuffer(PATH_PREFIX + "key_emitter");

				kEmitter.position(kEmitter.capacity() - 31);;
				String addrForEmitter  = Constants.GLOBAL_NAMESPACE + "01" + BlorkUtils.byteToHex(kEmitter);

				mockStateArray.put(addrForEmitter,  ByteString.copyFrom(new byte[] {0x42}));

				tx.hydrate(pou0);
				tx.process(mockState);
			};

			assertThrows(BlorkError.class, notRegistered);

		} catch (DecodeError e) {
			fail("Error : " + e.getMessage());
		}
	}
}
