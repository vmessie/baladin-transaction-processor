package com.orange.rd.blork;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.AbstractMap;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.bson.Document;
import org.bson.types.Binary;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import java.util.Arrays;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.BlorkUtils;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.DecodeError;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

class NewClientTransactionTest {
	static private NewClientTransaction tx;
	private Document data;

	@BeforeAll
	static public void register() {
		tx = new NewClientTransaction();
	}

	@BeforeEach
	public void populate() {
		this.data	= new Document();

		KeyPairGenerator keyGen;
		try {
			keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(2048);

			KeyPair keys = keyGen.genKeyPair();

			data.put("client_key", new Binary(keys.getPublic().getEncoded()));

			Signature signer = Signature.getInstance("SHA256withRSA");

			signer.initSign(keys.getPrivate());


			signer.update(keys.getPublic().getEncoded());
			data.put("client_sig", new Binary(signer.sign()));

			KeyFactory kf = KeyFactory.getInstance("RSA");
			PrivateKey pk = kf.generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode("MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCCSMnvxtwh3RZW" +
					"ny0FK5VuJ07W/ntKjho0JsENp59JjzBrCZRih1dgbdpdaGNgFrbqKgdwQGIt/9wU" +
					"nfnrSsaPFTPzeDfoemqjkV4HOpBmhMu6sWrIcWsTu+CBuchgnL8kNHAbLbSYFrp0" +
					"OnYF41EgKL47GkcnB+/UpjrmwfqnyAdxU41YrRCGGxNS9UUfG5p/R4llOa1XJZZx" +
					"xihL+DAMqYYNwNp+EneyHB2oGoes8b+0akiNGVLmjkMGFxsRrWV8smiFJ5AqnBQ6" +
					"nvSp105pqdVVYpg5B3yE7Yflpb8u8rNVjFiGgiZsjsyFaO4OGgRoEeOI2+Ep8PDO" +
					"rkfHJd1FAgMBAAECggEABGtRpjd5mhB0xIVrsoQy8mSU8Qf6yKGfU+uImtz3szgP" +
					"LWJKzTjIBvZS9N4omMbMd47rZ6xXaDYIkg51vrxvlcTjWTvXsaQNX/T7WoVrBn80" +
					"peWEuL3YIvg5ek0B9fz7ItBAiTt4p7wFoRGmlTaLtuqK4VhX14QWZ4En+FHWHcDH" +
					"1BFrYExrbwnzADTJ0W4syUpHjNf0djDMOFkl5FlF6WBIVcT1l0cSuFhrtjooffHV" +
					"NytnS37e7BdJAK/KZW0ENYuX7XD1ZLFORdLNf5op+pSD49W6UaP9pLKNdmJf0lYF" +
					"Opb0XhZv7K26VzkN0gmX8u5niMNJvchUaZI295uNwQKBgQC3tXPM3/XkLDmvxs0V" +
					"yThT0CUesKAOU8zvugo4yyvdzgYX8f66FSvAaJ/ouxVSjJJWF+bNUP8U8prkCwuC" +
					"n2f5xzt+vDTjlP6TWybMNRpaC+4FWSBpx4QdbgQ6aFXJto9AMSjrG5ZlShK0Dyc+" +
					"/cIDQASMZH2DsqeJa9RU91KpNQKBgQC1jW+melnsuup7Xh4MTUBqVH8GCq5uDa5I" +
					"Ypa2GKy8zsvE7s/nflwQX1lsA0LZrYVllJyIX+XpUKFd1RD/msKGtOlT19ERCzpa" +
					"2vgttOvB9KavH+MKs5REaIl4roEbvJTJyYGaq6nEOw3FNQfeYkov2dFnXD3U/lxh" +
					"/Cv+zxL10QKBgCRgULLDshvhSfbtZZSnyHiIIaGHFdpga5cYfoeqa2A7OGeramYM" +
					"ZSJvVSKvUNSMszlK+E+1dxz9wP/g1AGkcOE7uyMoP0zFJvekt/T9mt8zt4jlkg6A" +
					"DEALldi+6iXL+WeblF+hkdEyrqtqyLmnY7BjD8OA7n2Sdaw3Zs8APiT1AoGBAIuN" +
					"lG0bT1QwuVzrE53RI9qX6Kv0OfBOg9EQN1jxzpWzP9640wbWkl3jbREh0JkSUJ+s" +
					"hEYvsMKPP/qodNCFXVG+bjwMgJ/hSZHdDzfTAgWs0RN8J0FqmGWdFU62lHeFlbkm" +
					"M4F4wp4b5pHYGZxPYUc230fcF4tJmimXdRoAgkqBAoGAVn2pnjbJr1wNFXxml/PT" +
					"yiZuxhZqww/t28NzNb0gUShcx6zQ/CZD/9bxFjw2J86PADjt1ex+U35IYX+SjsfE" +
					"rfzDUjPxGg9dAy31ODDDeu1Evho2QvvrKl5yiySTLjr5TVNTaCROCJfab9g64tZC" +
					"pwu3Vd4NnVx9dyPWC9A9izA=")));


			signer.initSign(pk);

			signer.update(keys.getPublic().getEncoded());

			data.put("issuer_sig", new Binary(signer.sign()));
		} catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | SignatureException e1) {
			e1.printStackTrace();
			fail("Internal Error: " + e1.getMessage());
		}


		try {
			tx.hydrate(data);
		} catch (DecodeError e) {
			fail("La transaction a échouée : " + e.getMessage());
		}

	}

	@Test
	void testHydrate() {
		Executable malformedDocument = () -> {
			Document invalid = new Document();
			invalid.put("zeg", 15187);
			invalid.put("zzg", null);

			tx.hydrate(invalid);
		};

		Executable malformedDocument2 = () -> {
			data.put("client_key", 42);
			tx.hydrate(data);
		};

		Executable malformedDocument3 = () -> {
			data.put("client_sig", 42l);
			tx.hydrate(data);

		};

		Executable malformedDocument4 = () -> {
			data.put("issuer_sig", 0.42);
			tx.hydrate(data);
		};

		Executable invalidClientKey = () -> {
			data.put("client_key", new Binary(new byte[] {0x42}));
			tx.hydrate(data);
		};


		assertThrows(DecodeError.class, malformedDocument);
		assertThrows(DecodeError.class, malformedDocument2);
		assertThrows(DecodeError.class, malformedDocument3);
		assertThrows(DecodeError.class, malformedDocument4);

		assertThrows(DecodeError.class, invalidClientKey);


	}

	@Test
	void testToBSON() {
		assertEquals(data.toJson(), tx.toBSON().toJson());
	}

	@Test
	void testProcess() throws InternalError, InvalidTransactionException, DecodeError, BlorkError {


		String exceptedAddr = Constants.GLOBAL_NAMESPACE + "01" + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(((Binary) data.get("client_key")).getData(), ((Binary) data.get("client_key")).getData().length - 31, ((Binary) data.get("client_key")).getData().length)));

		Map<String, ByteString> toReturn = new HashMap<String, ByteString>();
		toReturn.put(exceptedAddr, ByteString.EMPTY);



		final State fakeStateNullBefore = Mockito.mock(State.class);
		Mockito.when(fakeStateNullBefore.getState(Collections.singleton(exceptedAddr))).thenThrow(InternalError.class);
		tx.process(fakeStateNullBefore);
		Mockito.verify(fakeStateNullBefore).setState(Collections.singleton(new AbstractMap.SimpleEntry<String, ByteString>(exceptedAddr, ByteString.copyFrom(new byte[] {0x01} ))));

		State fakeStateZeroBefore = Mockito.mock(State.class);
		toReturn.put(exceptedAddr, ByteString.copyFrom(new byte[] {0x00}));
		Mockito.when(fakeStateZeroBefore.getState(Collections.singleton(exceptedAddr))).thenReturn(toReturn);
		tx.process(fakeStateZeroBefore);
		Mockito.verify(fakeStateZeroBefore).setState(Collections.singleton(new AbstractMap.SimpleEntry<String, ByteString>(exceptedAddr, ByteString.copyFrom(new byte[] {0x01} ))));


		final State fakeStateOneBefore = Mockito.mock(State.class);
		Executable shouldFail = () -> {
			toReturn.put(exceptedAddr, ByteString.copyFrom(new byte[] {0x01}));
			Mockito.when(fakeStateOneBefore.getState(Collections.singleton(exceptedAddr))).thenReturn(toReturn);
			tx.process(fakeStateOneBefore);
		};

		assertThrows(BlorkError.class, shouldFail);
		Mockito.verify(fakeStateOneBefore, Mockito.never()).setState(Collections.singleton(new AbstractMap.SimpleEntry<String, ByteString>(exceptedAddr, ByteString.copyFrom(new byte[] {0x01}))));

		//invalid signatures

		final State fakeStateZeroBefore2 = Mockito.mock(State.class);
		toReturn.put(exceptedAddr, ByteString.copyFrom(new byte[] {0x00}));
		Mockito.when(fakeStateZeroBefore2.getState(Collections.singleton(exceptedAddr))).thenReturn(toReturn);



		final State fakeStateNullBefore2 = Mockito.mock(State.class);
		toReturn.put(exceptedAddr, ByteString.copyFrom(new byte[] {}));
		Mockito.when(fakeStateNullBefore2.getState(Collections.singleton(exceptedAddr))).thenThrow(InternalError.class);

		Binary oldClientSig = (Binary) data.get("client_sig");

		Executable shouldFail2 = () -> {

			data.put("client_sig", new Binary(new byte[] {0x42, 0x54 ,(byte) 0xef}));
			tx.hydrate(data);
			tx.process(fakeStateZeroBefore2);
		};

		assertThrows(BlorkError.class, shouldFail2);

		Mockito.verify(fakeStateZeroBefore2, Mockito.never()).setState(Collections.singleton(new AbstractMap.SimpleEntry<String, ByteString>(exceptedAddr, ByteString.copyFrom(new byte[] {0x01}))));

		Executable shouldFail3 = () -> {
			data.put("client_sig", oldClientSig);
			data.put("issuer_sig", new Binary(Base64.getDecoder().decode("dnEVLe8pY6A0CaKllTftr0Ku//uPkKxMviEB2BVSG1JY4hxrODRHM0juK0C3amsQ2jAP0K4iIxsg" +
					"+qnACAjQplrR+IIim0NZaFGVv9R3OI24PNSmhbJKeUbsI7E3U9aosIy3meaSauEEna3xoEfnjNLx" +
					"p9JHrff7bsYu/LU2bDyuqla4PyZn+6+U6aYOS4BVV5ekLJwxVsBRMPqIxqP8eqRXt0IBhawasiLs" +
					"J1HiVFjdrQPMX9im3EioOnOVAR+36c7PnWIm/DGyqGAOdh0xyy0PoJgnypSI1AQGCrjW8VBROcVj" +
					"MAY7jPeWvaZYIqkiQ6l3cIRMH80pQ0b9JSoSHg==")));

			tx.hydrate(data);

			tx.process(fakeStateNullBefore2);
		};

		assertThrows(BlorkError.class, shouldFail3);

		Mockito.verify(fakeStateNullBefore2, Mockito.never()).setState(Collections.singleton(new AbstractMap.SimpleEntry<String, ByteString>(exceptedAddr, ByteString.copyFrom(new byte[] {0x01}))));


	}
}
