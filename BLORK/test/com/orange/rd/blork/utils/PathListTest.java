package com.orange.rd.blork.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class PathListTest {
	private PublicKey pk1;
	private PublicKey pk2;
	private PublicKey pk3;
	private PublicKey pk4;
	private PublicKey pk5;
	private PublicKey pk6;
	private PublicKey pk7;
	private PublicKey pk8;
	
	private PathList toTest;
	
	
	@BeforeEach
	public void setup() {
		try {
			pk1 = new PublicKey(this.genRandomKey());
			pk2 = new PublicKey(this.genRandomKey());
			pk3 = new PublicKey(this.genRandomKey());
			pk4 = new PublicKey(this.genRandomKey());
			pk7 = new PublicKey(this.genRandomKey());
			pk8 = new PublicKey(this.genRandomKey());
			pk6 = pk1;
			pk5 = new PublicKey(ByteBuffer.wrap(pk2.getInstance().getEncoded()));
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void testPathListCollectionOfPublicKey() {
		Executable duplicateRelay1 = () -> {
			List<PublicKey> l = new ArrayList<PublicKey>();
			l.add(pk1);
			l.add(pk2);
			l.add(pk3);
			l.add(pk1);
			new PathList(l);
		};
		
		Executable duplicateRelay2 = () -> {
			List<PublicKey> l = new ArrayList<PublicKey>();
			l.add(pk1);
			l.add(pk2);
			l.add(pk3);
			l.add(pk6);
			new PathList(l);
		};
		
		Executable duplicateRelay3 = () -> {
			List<PublicKey> l = new ArrayList<PublicKey>();
			l.add(pk1);
			l.add(pk2);
			l.add(pk3);
			l.add(pk4);
			l.add(pk5);
			new PathList(l);
		};
		
		assertThrows(IllegalArgumentException.class, duplicateRelay1,
				"Duplicate object should throw an error");
		assertThrows(IllegalArgumentException.class, duplicateRelay2,
				"Duplicate object signature should throw an error");
		assertThrows(IllegalArgumentException.class, duplicateRelay3,
				"Duplicate key should throw an error");
		
		
		initList();
		
		assertEquals(pk1, toTest.get(0), "error while creating the list");
		assertEquals(pk2, toTest.get(1), "error while creating the list");
		assertEquals(pk3, toTest.get(2), "error while creating the list");
		assertEquals(pk4, toTest.get(3), "error while creating the list");
		
	}
	
	private void initList() {
		List<PublicKey> l = new ArrayList<PublicKey>();
		
		l.add(pk1);
		l.add(pk2);
		l.add(pk3);
		l.add(pk4);
		
		toTest = new PathList(l);
	}

	@Test
	void testAddPublicKey() {
		initList();
		assertEquals(false, toTest.add(pk1),
				"Duplicate object shuold throw an error");
		assertEquals(false, toTest.add(pk6),
				"Duplicate object signature should throw an error");
		assertEquals(false, toTest.add(pk5),
				"Duplicate key should throw an error");
		
		toTest.add(pk7);
		
		assertEquals(pk7, toTest.get(4), "Element not added");
		
	}

	@Test
	void testAddIntPublicKey() {
		initList();
		Executable case1 = () -> toTest.add(0, pk1);
		Executable case2 = () -> toTest.add(0, pk6);
		Executable case3 = () -> toTest.add(0, pk5);
		
		assertThrows(IllegalArgumentException.class, case1,
				"Duplicate object should throw an error");
		assertThrows(IllegalArgumentException.class, case2,
				"Duplicate object signature should throw an error");
		assertThrows(IllegalArgumentException.class, case3,
				"Duplicate key should throw an error");
		
		toTest.add(1, pk7);
		
		assertEquals(pk7, toTest.get(1), "Element not added");
		assertEquals(pk2, toTest.get(2), "Element misplaced");
	}

	@Test
	void testAddAllCollectionOfQextendsPublicKey() {
		List<PublicKey> colToAdd = new LinkedList<PublicKey>();
		colToAdd.add(pk7);
		colToAdd.add(pk8);
		
		initList();
		
		Executable case1 = () -> {
			colToAdd.set(0, pk1);
			colToAdd.set(1, pk2);
			if (toTest.addAll(colToAdd) == false) throw new IllegalArgumentException();
		};
		Executable case2 = () -> {
			colToAdd.set(0, pk6);
			colToAdd.set(1, pk2);
			if (toTest.addAll(colToAdd) == false) throw new IllegalArgumentException();
			
		};
		Executable case3 = () -> {
			colToAdd.set(0, pk5);
			colToAdd.set(1, pk6);
			if (toTest.addAll(colToAdd) == false) throw new IllegalArgumentException();
		};
		
		assertThrows(IllegalArgumentException.class, case1,
				"Duplicate object should throw an error");
		assertThrows(IllegalArgumentException.class, case2,
				"Duplicate object signature should throw an error");
		assertThrows(IllegalArgumentException.class, case3,
				"Duplicate key should throw an error");
		
		List<PublicKey> colToAdd2 = new LinkedList<PublicKey>();
		colToAdd2.add(pk7);
		colToAdd2.add(pk8);
		
		initList();
		
		
		assertEquals(true, toTest.addAll(colToAdd2), "Should be successful");
		
		assertEquals(pk7, toTest.get(4), "Element not added");
		assertEquals(pk8, toTest.get(5), "Element not added");
	}
	
	@Test
	void testAddAllIntCollectionOfQextendsPublicKey() {
		List<PublicKey> colToAdd = new LinkedList<PublicKey>();
		colToAdd.add(pk7);
		colToAdd.add(pk8);
		
		initList();
		
		Executable case1 = () -> {
			colToAdd.set(0, pk1);
			colToAdd.set(1, pk2);
			if (toTest.addAll(3, colToAdd) == false) throw new IllegalArgumentException();
		};
		Executable case2 = () -> {
			colToAdd.set(0, pk6);
			colToAdd.set(1, pk2);
			if (toTest.addAll(0, colToAdd) == false) throw new IllegalArgumentException();
			
		};
		Executable case3 = () -> {
			colToAdd.add(pk5);
			colToAdd.set(0, pk6);
			if (toTest.addAll(2, colToAdd) == false) throw new IllegalArgumentException();
		};
		
		assertThrows(IllegalArgumentException.class, case1,
				"Duplicate object should throw an error");
		assertThrows(IllegalArgumentException.class, case2,
				"Duplicate object signature should throw an error");
		assertThrows(IllegalArgumentException.class, case3,
				"Duplicate key should throw an error");
		
		
		List<PublicKey> colToAdd2 = new LinkedList<PublicKey>();
		colToAdd2.add(pk7);
		colToAdd2.add(pk8);
		
		assertEquals(true, toTest.addAll(1, colToAdd2), "the operation should have been successful");
		
		assertEquals(pk7, toTest.get(1), "Element not added");
		assertEquals(pk8, toTest.get(2), "Element not added");
		assertEquals(pk2, toTest.get(3), "Element misplaced");
	}

	@Test
	void testSetIntPublicKey() {
		initList();
		Executable case1 = () -> toTest.set(1, pk1);
		Executable case2 = () -> toTest.set(1, pk6);
		Executable case3 = () -> toTest.set(0, pk5);
		
		assertThrows(IllegalArgumentException.class, case1,
				"Duplicate object should throw an error");
		assertThrows(IllegalArgumentException.class, case2,
				"Duplicate object signature should throw an error");
		assertThrows(IllegalArgumentException.class, case3,
				"Duplicate key should throw an error");
		
		toTest.set(1, pk7);
		
		assertEquals(pk7, toTest.get(1), "Element not added");
		assertEquals(pk3, toTest.get(2), "Element misplaced");
	}
	
	private ByteBuffer genRandomKey() {
		try {
			KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
			gen.initialize(2048);
			
			KeyPair pair = gen.genKeyPair();
			
			return ByteBuffer.wrap(pair.getPublic().getEncoded());
			
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
		
	}

}
