package com.orange.rd.blork.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import java.util.Random;

import org.junit.jupiter.api.Test;

class NonceTest {
	private Random r;
	private Nonce n;
	private long nonceToTest;
	public NonceTest() {
		r = new Random();
		nonceToTest = r.nextLong();
		System.out.println("Nonce to test : " + r.nextLong());
	}
	
	@Test 
	void testNonce() {
		
		ByteBuffer bf = ByteBuffer.allocate(8);
		bf.putLong(nonceToTest);
		n = new Nonce(bf);
	}

	@Test
	void testNonceLong() {
		testNonce();
		assertEquals(nonceToTest, n.getNonce(), "Error : different nonce!");
	}

	@Test
	void testnonceByte() {
		testNonce();
		ByteBuffer bf = ByteBuffer.allocate(8);
		bf.putLong(nonceToTest);
		assertArrayEquals(bf.array(), n.getNonceBytes().array(), "Error : different nonce!");
	}

}
