package com.orange.rd.blork.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;

import com.google.protobuf.ByteString;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

import org.junit.jupiter.api.Test;

class BlorkUtilsTest {
	@Test
	void testHexToBytes() throws DecodeError {
		Executable invalidString = () -> BlorkUtils.hexToBytes("INVALID");
		Executable invalidHex    = () -> BlorkUtils.hexToBytes("0x4F54EZ");
		Executable wrongSize	 = () -> BlorkUtils.hexToBytes("12345FE");

		assertThrows(DecodeError.class, invalidString, "String mal formed : error should be triggered");
		assertThrows(DecodeError.class, invalidHex,    "String mal formed : error should be triggered");
		assertThrows(DecodeError.class, wrongSize,     "Bad string size : error should be triggered");


		assertArrayEquals(new byte[] {0x01, (byte) 0xFF, 0x4E, 0x45}, BlorkUtils.hexToBytes("0x01FF4E45").array(), "Error decoding simple Hex");
		assertArrayEquals(new byte[] {0x4E, (byte) 0x87, 0x00, (byte) 0xFE}, BlorkUtils.hexToBytes("4E8700fE").array(), "Error decoding complex Hex");

		//fail("Not yet implemented");
	}

	@Test
	void testBytesToHex() {
		assertEquals("012d34ae", BlorkUtils.byteToHex(ByteBuffer.wrap(new byte[] {0x01,0x2d, 0x34, (byte) 0xae})));
	}

	@Test
	void testIsClientRegistered() {
		KeyPairGenerator keyGen;
		String k1Addr = "";
		String k2Addr = "";
		String k3Addr = "";

		PublicKey k1 = null;
		PublicKey k2 = null;
		PublicKey k3 = null;

		try {
			keyGen = KeyPairGenerator.getInstance("RSA");

			keyGen.initialize(2048);

			KeyPair keys1 = keyGen.genKeyPair();
			KeyPair keys2 = keyGen.genKeyPair();
			KeyPair keys3 = keyGen.genKeyPair();

			k1Addr = Constants.GLOBAL_NAMESPACE + "01" + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(keys1.getPublic().getEncoded(), keys1.getPublic().getEncoded().length - 31, keys1.getPublic().getEncoded().length)));
			k2Addr = Constants.GLOBAL_NAMESPACE + "01" + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(keys2.getPublic().getEncoded(), keys2.getPublic().getEncoded().length - 31, keys2.getPublic().getEncoded().length)));
			k3Addr = Constants.GLOBAL_NAMESPACE + "01" + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(keys3.getPublic().getEncoded(), keys3.getPublic().getEncoded().length - 31, keys3.getPublic().getEncoded().length)));

			k1 = new PublicKey(ByteBuffer.wrap(keys1.getPublic().getEncoded()));
			k2 = new PublicKey(ByteBuffer.wrap(keys2.getPublic().getEncoded()));
			k3 = new PublicKey(ByteBuffer.wrap(keys3.getPublic().getEncoded()));

		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
			fail("Internal error : " + e.getMessage());
		}




		Map<String, ByteString> toReturnRegistered = new HashMap<String, ByteString>();
		toReturnRegistered.put(k1Addr, ByteString.copyFrom(new byte[] {0x01}));

		Map<String, ByteString> toReturnUnregistered1 = new HashMap<String, ByteString>();
		toReturnUnregistered1.put(k2Addr, ByteString.copyFrom(new byte[] {0x00}));

		Map<String, ByteString> toReturnUnregistered2 = new HashMap<String, ByteString>();
		toReturnUnregistered2.put(k3Addr, ByteString.EMPTY);

		State mockState = Mockito.mock(State.class);
		try {
			Mockito.when(mockState.getState(Collections.singleton(k1Addr))).thenReturn(toReturnRegistered);
			Mockito.when(mockState.getState(Collections.singleton(k2Addr))).thenReturn(toReturnUnregistered1);
			Mockito.when(mockState.getState(Collections.singleton(k3Addr))).thenReturn(toReturnUnregistered2);

		} catch (InternalError | InvalidTransactionException e) {
			fail("Internal error ! " + e.getMessage());
			e.printStackTrace();
		}

		assertTrue(BlorkUtils.isClientRegistered(mockState, k1));

		assertFalse(BlorkUtils.isClientRegistered(mockState, k2));
		assertFalse(BlorkUtils.isClientRegistered(mockState, k3));
	}

	@Test
	void testIsRelayRegistered() {
		KeyPairGenerator keyGen;
		String k1Addr = "";
		String k2Addr = "";
		String k3Addr = "";

		PublicKey k1 = null;
		PublicKey k2 = null;
		PublicKey k3 = null;

		try {
			keyGen = KeyPairGenerator.getInstance("RSA");

			keyGen.initialize(2048);

			KeyPair keys1 = keyGen.genKeyPair();
			KeyPair keys2 = keyGen.genKeyPair();
			KeyPair keys3 = keyGen.genKeyPair();


			k1Addr = Constants.GLOBAL_NAMESPACE + "02" + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(keys1.getPublic().getEncoded(), keys1.getPublic().getEncoded().length - 31, keys1.getPublic().getEncoded().length)));
			k2Addr = Constants.GLOBAL_NAMESPACE + "02" + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(keys2.getPublic().getEncoded(), keys2.getPublic().getEncoded().length - 31, keys2.getPublic().getEncoded().length)));
			k3Addr = Constants.GLOBAL_NAMESPACE + "02" + BlorkUtils.byteToHex(ByteBuffer.wrap(Arrays.copyOfRange(keys3.getPublic().getEncoded(), keys3.getPublic().getEncoded().length - 31, keys3.getPublic().getEncoded().length)));

			k1 = new PublicKey(ByteBuffer.wrap(keys1.getPublic().getEncoded()));
			k2 = new PublicKey(ByteBuffer.wrap(keys2.getPublic().getEncoded()));
			k3 = new PublicKey(ByteBuffer.wrap(keys3.getPublic().getEncoded()));

		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
			fail("Internal error : " + e.getMessage());
		}




		Map<String, ByteString> toReturnRegistered = new HashMap<String, ByteString>();
		toReturnRegistered.put(k1Addr, ByteString.copyFrom(new byte[] {0x01}));

		Map<String, ByteString> toReturnUnregistered1 = new HashMap<String, ByteString>();
		toReturnUnregistered1.put(k2Addr, ByteString.copyFrom(new byte[] {0x00}));

		Map<String, ByteString> toReturnUnregistered2 = new HashMap<String, ByteString>();
		toReturnUnregistered2.put(k3Addr, ByteString.EMPTY);

		State mockState = Mockito.mock(State.class);
		try {
			Mockito.when(mockState.getState(Collections.singleton(k1Addr))).thenReturn(toReturnRegistered);
			Mockito.when(mockState.getState(Collections.singleton(k2Addr))).thenReturn(toReturnUnregistered1);
			Mockito.when(mockState.getState(Collections.singleton(k3Addr))).thenReturn(toReturnUnregistered2);
		} catch (InternalError | InvalidTransactionException e) {
			fail("Internal error ! " + e.getMessage());
			e.printStackTrace();
		}

		assertTrue(BlorkUtils.isRelayRegistered(mockState, k1));

		assertFalse(BlorkUtils.isRelayRegistered(mockState, k2));
		assertFalse(BlorkUtils.isRelayRegistered(mockState, k3));
	}
}
