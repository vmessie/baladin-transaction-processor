package com.orange.rd.blork.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import org.junit.jupiter.api.Test ;

class PublicKeyTest {
	/*final String testPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlzUns1DNVbs6hwrDZ24f" + 
			"EhoHsFhrU6kkYD8nw7xwUQPbegOoJ+FWaUKuBpSVBWBvj/q5f6D7vpsoDpfBGeet" + 
			"atYOsuwmviCcYcd4w00w6OC6lrS3DZ68DNS0yF6saB1eK5dE81T9lCJODB6Y9c14" + 
			"hOjewd4f08XGqsmmRCRJyg/uFK4QitUfVv/O7vvH2IynLN5BkBEYd0OXrOXE9tYK" + 
			"AVPBE0BoSMAKjbMAvmT0EvDsG3xwc8Vku7xTghGom5FO3jH8CK4qWCltiPcnQ7Ou" + 
			"HWUw/7+7JJ1NW2d59lHxF4Rj+T8GwstnMMGVKyAV5tCOIEj4xF9R5kWoufzNwmEe" + 
			"qwIDAQAB";*/
	final String testPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyvvVAkZzEVzhRl4UKJ7U/3YukU2Fugma" + 
			"2s0clzDj+fNPwY2tka7BGrEl2qbeW8vicqdmxzV+62FyFkrokh0/BR1TjEw9hON5e+eD6n8lQ/Tc" + 
			"xkLX0nlbJ4cxyZOgtSpj/R2c2a4fDK0vcP3qmX5j+Qzxun2f2m5mYlMSkajkuwgmSQlJQyWhScsX" + 
			"PDk752ON5trCrCb+8Nn45F8UggSGivPSkxDwEVv0eVUJgixX9g+tqPn7a4d2u9tCiXp3p8OVzxhT" + 
			"ngqW+dlLV3TIx+DpZam2IfG/h6t15pMBhoclQQabkqGwmvMWQyfuXTBOADXIPRvIypQxyN2k44um" + 
			"amIkkQIDAQAB";
	
	@Test
	void testPublicKeyByteBuffer() throws NoSuchAlgorithmException {
		byte[] key = Base64.getDecoder().decode(testPublicKey);
		ByteBuffer keyBuf = ByteBuffer.wrap(key);
		
		
		
		try {
			PublicKey keyO = new PublicKey(keyBuf);
			Signature test = Signature.getInstance("SHA256withRSA");
			test.initVerify(keyO.getInstance());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			fail("Error");
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			fail("The key is invalid");
		} catch (InvalidKeySpecException e) {
			fail("The key is invalid!");
			e.printStackTrace();
		} 
	}
}
