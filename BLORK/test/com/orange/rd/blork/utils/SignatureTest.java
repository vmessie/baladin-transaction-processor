package com.orange.rd.blork.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;

import org.junit.jupiter.api.Test;

class SignatureTest {
	final String signedText = "TEST TEST TEST";
	final String pk         = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwoUSjAc3EUPSkzcNNmvobBTn0x7ETqcY" + 
			"oVNKl+SxonroxieuDyjRfzP2saSiZ8AN3vpjbAzuvRZVHOsGE3pho1bhH0O+UkJ9qFO1kUPioU4J" + 
			"Ih4Ghbu+Z5ADWPsjdw8GCtWteKYJAFcZKfjxHQCb0V4hYCOyR09gfJo1PZdIFE262UDKXdvjApkC" + 
			"gDkzK+llMRD2hEQV6nWmUrVEDx1P+NnXeg+FacH/Kd8+6C33A9s5f+3MSBlycAWm/B1vPWh4FlPM" + 
			"8O5cCfst/EKHtMy7c3Sfo+cAaMF1t3GhlwaWO1rp7m6DDA4bQH8BpdXZpNSrCTORi15Pw9eNPL76" + 
			"z4Zv0wIDAQAB";
	final String signature = "oHB4l4YKbF+KIMp17SX0iZKzdjxzUnCKFDFGuQVdi+8Azlohcr2xholRPnLw0SLDM9jCv6XxEbJa" + 
			"qCvw60+CO8V+VpL94m1R0cYw7JKXLGMrd9+YyxADo99F1sr50R9eZlLJq7YBqhLGM+eibzng5jlr" + 
			"SACQZFAyMbxOAD9J33NN77KK5Jn5riWNMK4kKz68bnsPQI+qHGLdoP84Irnsq0ieG6Z5QfyCkJcF" + 
			"gQ3hD9PEz92U+zf1KpXNBQwQ6Ao3PInfSg4XYx6D5eBzAVMd+Aa+FJEB/0Aih4gbkNzBc508e51c" + 
			"wAw9HsMPDgOsD8XnldNK6T+jkjC9xjoVTVfMTQ==";
	
	private Signature sig;
	
	
	@Test
	void testSignature() {
		ByteBuffer sigBuffer = ByteBuffer.wrap(Base64.getDecoder().decode(signature));
		
		ByteBuffer pkBuf	 = ByteBuffer.wrap(Base64.getDecoder().decode(pk));
		
		PublicKey pk;
		try {
			pk = new PublicKey(pkBuf);
			sig = new Signature(sigBuffer, pk);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
			fail("Critical error!");
		}
	}

	@Test
	void testIsValid() {
		testSignature();
		ByteBuffer textBuf	 = ByteBuffer.wrap(signedText.getBytes());
		assertEquals(true,  sig.isValid(textBuf), "Signature should be valid");
		
		ByteBuffer textBf2   = ByteBuffer.wrap(signedText.substring(2).getBytes());
		assertEquals(false, sig.isValid(textBf2), "Signature should be invalid");
		
	}
}
