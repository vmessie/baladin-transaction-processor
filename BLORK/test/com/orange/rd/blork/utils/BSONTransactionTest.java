/**
 * 
 */
package com.orange.rd.blork.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import org.bson.BsonBinaryWriter;
import org.bson.Document;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.io.BasicOutputBuffer;
import org.bson.io.OutputBuffer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import sawtooth.sdk.processor.State;

import org.mockito.Mockito;

/**
 * @author lsmv2780
 *
 */

//@RunWith(MockitoJUnitRunner.class)
class BSONTransactionTest {
	public static int id; 
	public static boolean success =true;
	private BSONTransactionMockTest tx;
	
	/**
	 * Test method for {@link com.orange.rd.blork.utils.BSONTransaction#registerTransaction()}.
	 */
	@Test
	void testRegisterTransaction() {
		id = 0x48;
		tx= Mockito.spy(new BSONTransactionMockTest());
		tx.triggerRegister();
		Mockito.verify(tx, Mockito.atLeastOnce()).getId();
		
		assertNotNull(BSONTransaction.getTransactionType(0x48));
	}

	/**
	 * Test method for {@link com.orange.rd.blork.utils.BSONTransaction#incomingTransaction(java.nio.ByteBuffer)}.
	 */
	@Test
	void testIncomingTransaction() {
		Executable notValidBSON 		= () -> BSONTransaction.incomingTransaction(ByteBuffer.allocate(8).putLong(42), null);
		Executable invalidBsonFormat1	= () -> {
			Document d = new Document();
			d.put("egoner", "654");
			d.put("545", 13254l);
			
			DocumentCodec codec 	= new DocumentCodec();
			
			OutputBuffer output = new BasicOutputBuffer();
			BsonBinaryWriter writer = new BsonBinaryWriter(output);
			
			
			codec.encode(writer, d, EncoderContext.builder().build());
			
			BSONTransaction.incomingTransaction(ByteBuffer.wrap(output.toByteArray()), null);
			
		};
		
		Executable invalidBsonFormat2 = () -> {
			Document d = new Document();
			d.put("id", (byte) 0x42);
			d.put("anotherKey", "ded");
			
			DocumentCodec codec 	= new DocumentCodec();
			
			OutputBuffer output = new BasicOutputBuffer();
			BsonBinaryWriter writer = new BsonBinaryWriter(output);
			
			
			codec.encode(writer, d, EncoderContext.builder().build());
			
			BSONTransaction.incomingTransaction(ByteBuffer.wrap(output.toByteArray()), null);
		};
		

		Executable invalidBsonFormat3 = () -> {
			Document d = new Document();
			d.put("id", (byte) 0x42);
			d.put("body", "not a document");
			
			DocumentCodec codec 	= new DocumentCodec();
			
			OutputBuffer output = new BasicOutputBuffer();
			BsonBinaryWriter writer = new BsonBinaryWriter(output);
			
			
			codec.encode(writer, d, EncoderContext.builder().build());
			
			BSONTransaction.incomingTransaction(ByteBuffer.wrap(output.toByteArray()), null);
		};
		
		Executable notRegisteredTransaction = () -> {
			Document d = new Document();
			d.put("id", (byte) 0x42);
			d.put("body", new Document());
			
			DocumentCodec codec 	= new DocumentCodec();
			
			OutputBuffer output = new BasicOutputBuffer();
			BsonBinaryWriter writer = new BsonBinaryWriter(output);
			
			
			codec.encode(writer, d, EncoderContext.builder().build());
			
			BSONTransaction.incomingTransaction(ByteBuffer.wrap(output.toByteArray()), null);
		};
		
		Executable failedTransaction = () -> {
			id = 0x42;
			tx = new BSONTransactionMockTest();
			tx.triggerRegister();
			success = false;
			
			
			Document d 		= new Document();
			Document body	= new Document();
			body.put("test", "test string");
			d.put("id", 0x42);
			d.put("body", body);
			
			DocumentCodec codec 	= new DocumentCodec();
			
			OutputBuffer output = new BasicOutputBuffer();
			BsonBinaryWriter writer = new BsonBinaryWriter(output);
			
			
			codec.encode(writer, d, EncoderContext.builder().build());
			

			BSONTransaction.incomingTransaction(ByteBuffer.wrap(output.toByteArray()), null);
		};
		
		assertThrows(DecodeError.class, notValidBSON);
		assertThrows(DecodeError.class, invalidBsonFormat1);
		assertThrows(DecodeError.class, invalidBsonFormat2);
		assertThrows(DecodeError.class, invalidBsonFormat3);
		assertThrows(DecodeError.class, notRegisteredTransaction);
		
		assertThrows(BlorkError.class, failedTransaction);
		
		id = 0x43;
		tx= Mockito.spy(new BSONTransactionMockTest());
		tx.triggerRegister();
		
		Document d 		= new Document();
		Document body	= new Document();
		body.put("test", "test string");
		d.put("id", 0x43);
		d.put("body", body);
		
		DocumentCodec codec 	= new DocumentCodec();
		
		OutputBuffer output = new BasicOutputBuffer();
		BsonBinaryWriter writer = new BsonBinaryWriter(output);
		
		
		codec.encode(writer, d, EncoderContext.builder().build());
		success = true;
		
		try {
			BSONTransaction.incomingTransaction(ByteBuffer.wrap(output.toByteArray()), null);
			Mockito.verify(tx).hydrate(Mockito.any(Document.class));
		} catch (DecodeError | BlorkError e) {
			fail("exception has been raised : " + e.getMessage());
		}		
	}
	
}

class BSONTransactionMockTest extends BSONTransaction {
	public BSONTransactionMockTest() {
		super();
	}
	
	@Override
	public void hydrate(Document d) throws DecodeError {
	
	}

	@Override
	public Document toBSON() {
		return null;
	}

	@Override
	public void process(State state) throws BlorkError{
		if(!BSONTransactionTest.success) throw new BlorkError("ERROR");
	}

	@Override
	public int getId() {
		
		return BSONTransactionTest.id;
	}
	
	@Override
	protected void registerTransaction() {
		//DO litteraly nothing
	}
	/**
	 * For testing
	 */
	public void triggerRegister() {
		super.registerTransaction();
	}
}
