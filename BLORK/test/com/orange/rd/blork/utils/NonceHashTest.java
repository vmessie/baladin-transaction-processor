package com.orange.rd.blork.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import java.util.Random;

import org.junit.jupiter.api.Test;

class NonceHashTest {
	private long nonceToTest;
	private NonceHash hash;
	private ByteBuffer expectedHash;
	public NonceHashTest() {
		Random r = new Random();
		nonceToTest = r.nextLong();
		System.out.println("Testing with value " + nonceToTest);
		
	}
	
	@Test
	void testCreateFromPayload() {
		ByteBuffer bf = ByteBuffer.allocate(8);
		bf.rewind();
		bf.putLong(nonceToTest);
		hash = NonceHash.createFromPayload(bf);
	}
	
	@Test
	void testGetHash() {
		testCreateFromPayload();
		expectedHash = hash.getHash();
	}
	
	
	@Test
	void testNonceHashByteBuffer() {
		testGetHash();
		NonceHash otherHash = new NonceHash(expectedHash);
		assertArrayEquals(hash.getHash().array(), otherHash.getHash().array(),
				"Error : hashes should be the sames!");
	}

	@Test
	void testIsHashValid() {
		testCreateFromPayload();
		ByteBuffer bf = ByteBuffer.allocate(8);
		bf.rewind();
		bf.putLong(nonceToTest);
		assertEquals(true, hash.isHashValid(bf), 
				"Error : the hash should be valid");
	}
}
