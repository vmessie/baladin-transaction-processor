package com.orange.rd.blork;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.protobuf.ByteString;
import com.orange.rd.blork.utils.BlorkError;
import com.orange.rd.blork.utils.Constants;
import com.orange.rd.blork.utils.Nonce;
import com.orange.rd.blork.utils.PathList;
import com.orange.rd.blork.utils.PublicKey;
import com.orange.rd.blork.utils.Signature;

import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

class PoUTest {
	final String PATH_PREFIX = "/tmp/py_pou_test/";
	
	
	private List<PoU> PoUs;
	
	private Path path;
	boolean genPoU = true;


	State mockState;
	Map<String, ByteString> mockStateArray;


	private boolean firstOccur = true; //pour être sur de ne réinitialiser l'état qu'une seule fois par test
	
	private ByteBuffer file2ByteBuffer(String path) throws IOException {
		
		InputStream stream 	= new FileInputStream(path);

		int byteRead;
		byte[] buffer = new byte[1000];
		
		
		int position = 0;
		
		while ((byteRead  = stream.read()) != -1) {
			buffer[position] = (byte) byteRead;
			position++;
		}
		
		stream.close();
		
		return ByteBuffer.wrap(Arrays.copyOfRange(buffer, 0 ,position));
	}
	@BeforeEach
	public void initializeSomePoU() throws BlorkError {
		if (firstOccur ) {
			mockState = Mockito.mock(State.class);
			mockStateArray = new HashMap<>();
			
			try {
				Mockito.when(mockState.getState(Mockito.any())).thenAnswer(new Answer<Map<String,ByteString>>() {

					@Override
					public Map<String, ByteString> answer(InvocationOnMock invocation) throws Throwable {
						Collection<String> wantedAddr = null;
						Object arg = invocation.getArgument(0);

						if (!(arg instanceof Collection<?>)) throw new Throwable(arg.toString());
						
						else  wantedAddr = (Collection<String> ) arg;
						
						//On simule l'enregistrement de n'importe quel client / relais
						for (String w : wantedAddr) {
							if (w.substring(0, 8).equals(Constants.GLOBAL_NAMESPACE + "01") || w.substring(0, 8).equals(Constants.GLOBAL_NAMESPACE + "02")) {							
								mockStateArray.put(w, ByteString.copyFrom(new byte[] {0x01}));
							}
						}
						return mockStateArray;
					}
					
				});
				Mockito.when(mockState.setState(Mockito.any())).thenAnswer(new Answer<Collection<String>>() {
					@Override
					public Collection<String> answer(InvocationOnMock invocation) throws Throwable {
						Collection<Map.Entry<String, ByteString>> stateMap = null;
						Object arg = invocation.getArgument(0);
	
						if (!(arg instanceof Collection<?>)) throw new Throwable(arg.toString());
						
						else  stateMap = (Collection<Map.Entry<String, ByteString>> ) arg;
						Set<String> keySet = new HashSet<String>();
						
						for (Map.Entry<String, ByteString> k : stateMap) {
							mockStateArray.put(k.getKey(), k.getValue());
							keySet.add(k.getKey());
						}
						
						return keySet;
					}
				});
			} catch (InternalError | InvalidTransactionException e) {
				fail("Internal error! ");
				e.printStackTrace();
			}
		}
		if (genPoU) {
			try {
				Process p = Runtime.getRuntime().exec("../py_gen_pou/main_bson.py");
				p.waitFor();
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
				fail("Cannot generate PoUs");
			}
		}
		
		
		
		
		PublicKey emitterPK;
		try {
			emitterPK = new PublicKey(file2ByteBuffer(PATH_PREFIX + "key_emitter"));
			
			PublicKey receiverPK	= new PublicKey(file2ByteBuffer(PATH_PREFIX + "key_receiver"));
			
			PathList relays = new PathList();
				
			relays.add(new PublicKey(file2ByteBuffer(PATH_PREFIX + "key_relay1")));
			relays.add(new PublicKey(file2ByteBuffer(PATH_PREFIX + "key_relay2")));
			relays.add(new PublicKey(file2ByteBuffer(PATH_PREFIX + "key_relay3")));
			relays.add(new PublicKey(file2ByteBuffer(PATH_PREFIX + "key_relay4")));
	
			ByteBuffer pathId		= file2ByteBuffer((PATH_PREFIX + "path_id"));
			
			PoUs = new LinkedList<PoU>();
			
			for (int i = 0; i < 4 ; i++) {
				ByteBuffer headers = file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_headers");  
				
				headers.rewind();
				
				int number 				= headers.getInt();
				long timestamp 			= headers.getLong();
				
				headers.position(headers.position() + 16); // on saute le pathId
				
				int nextFrame 			= headers.getInt();
				long dataConsumption	= headers.getLong();
				
				Nonce emitterNonce 		= new Nonce(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_nonce_emitter"));
				
				Signature emitterSignatureOut	= new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_out_emitter"), emitterPK);
				Signature emitterSignatureBack	= new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_back_emitter"), emitterPK);
				Signature receiverSignature		= new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_receiver"), receiverPK);
				
				
				
				Map<PublicKey, Nonce> rlyNonce = new HashMap<PublicKey, Nonce>();
				
				rlyNonce.put(relays.get(0), new Nonce(file2ByteBuffer(PATH_PREFIX +"pou_" + i + "_nonce_relay1")));
				rlyNonce.put(relays.get(1), new Nonce(file2ByteBuffer(PATH_PREFIX +"pou_" + i + "_nonce_relay2")));
				rlyNonce.put(relays.get(2), new Nonce(file2ByteBuffer(PATH_PREFIX +"pou_" + i + "_nonce_relay3")));
				rlyNonce.put(relays.get(3), new Nonce(file2ByteBuffer(PATH_PREFIX +"pou_" + i + "_nonce_relay4")));
				
				Map<PublicKey, Signature> rlySigOut = new HashMap<PublicKey, Signature>();	
				
				rlySigOut.put(relays.get(0), new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_out_relay1"), relays.get(0)));
				rlySigOut.put(relays.get(1), new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_out_relay2"), relays.get(1)));
				rlySigOut.put(relays.get(2), new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_out_relay3"), relays.get(2)));
				rlySigOut.put(relays.get(3), new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_out_relay4"), relays.get(3)));
				
				Map<PublicKey, Signature> rlySigBack = new HashMap<PublicKey, Signature>();	
			
				rlySigBack.put(relays.get(0), new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_back_relay1"), relays.get(0)));
				rlySigBack.put(relays.get(1), new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_back_relay2"), relays.get(1)));
				rlySigBack.put(relays.get(2), new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_back_relay3"), relays.get(2)));
				rlySigBack.put(relays.get(3), new Signature(file2ByteBuffer(PATH_PREFIX + "pou_" + i + "_sig_back_relay4"), relays.get(3)));
				
				PoUs.add(new PoU(
						timestamp,
						nextFrame,
						dataConsumption,
						number,
						(i == 0)?null:pathId,
						relays,
						rlyNonce,
						rlySigOut,
						rlySigBack,
						emitterNonce,
						emitterPK,
						emitterSignatureOut,
						emitterSignatureBack,
						receiverPK,
						receiverSignature,
						mockState));
			}
			
			this.path = Path.getPathById(pathId, mockState);
		} catch (InvalidKeySpecException | IOException | BlorkError e) {
			if (genPoU == false && e.getClass() == BlorkError.class) throw new BlorkError(e.getMessage());
			e.printStackTrace();
			fail("An error has been thrown : " + e.getMessage());
			
		}
		
		firstOccur = false;
	}
	
	@Test
	void testPoU() throws InvalidKeySpecException, IOException, BlorkError {
		initializeSomePoU(); //deuxième essai, pour pêtre sur que OK (path différent)
		
		this.genPoU = false;
		
		Executable duplicatePoU = () -> initializeSomePoU();
		
		Executable wrongNumber = () -> {
			this.genPoU = true;
			initializeSomePoU();
			this.genPoU = false;
			ByteBuffer headers = file2ByteBuffer(PATH_PREFIX+ "pou_1_headers");
			
			headers.position(0);
			headers.putInt(18);
			
			try (FileOutputStream fos = new FileOutputStream(PATH_PREFIX+ "pou_1_headers")) {
				fos.write(headers.array());
				fos.close();
			}
			
			initializeSomePoU();
		};
		
		Executable wrongNonceHash = () -> {
			this.genPoU = true;
			initializeSomePoU();
			this.genPoU = false;
			
			ByteBuffer nonce = file2ByteBuffer(PATH_PREFIX + "pou_1_nonce_emitter");
			nonce.put((byte) 0x42); // on modifie le nonce
			
			try (FileOutputStream fos = new FileOutputStream(PATH_PREFIX+ "pou_1_nonce_emitter")) {
				fos.write(nonce.array());
				fos.close();
			}
			initializeSomePoU();
			
		};
		
		Executable wrongSig = () -> {
			this.genPoU = true;
			initializeSomePoU();
			this.genPoU = false;
			
			ByteBuffer sig = file2ByteBuffer(PATH_PREFIX + "pou_1_sig_back_emitter");
			sig.put((byte) 0x42); // on modifie la signature
			
			try (FileOutputStream fos = new FileOutputStream(PATH_PREFIX+ "pou_1_sig_back_emitter")) {
				fos.write(sig.array());
				fos.close();
			}
			initializeSomePoU();
		};
		 
		assertThrows(BlorkError.class, duplicatePoU);
		assertThrows(BlorkError.class, wrongNumber);
		assertThrows(BlorkError.class, wrongSig);
		assertThrows(BlorkError.class, wrongNonceHash);
	}

	@Test
	void testGetPath() throws InvalidKeySpecException, IOException, BlorkError {
		assertEquals(this.path, this.PoUs.get(0).getPath(), "Path should be the same");
	}
}
