#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os

PATH_PREFIX = "/tmp/py_pou_test/"

if not os.path.exists(PATH_PREFIX):
	os.makedirs(PATH_PREFIX)

from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15 as Signer

from struct import pack,unpack

from random import randint

from time import time

import bson

# import base64 #for debug

print("\033[33mGénération paires de clés RSA...\033[00m")
#Gen some accounts
keyEmitter 	= RSA.generate(2048)
keyReceiver	= RSA.generate(2048)
keyRelay1	= RSA.generate(2048)
keyRelay2	= RSA.generate(2048)
keyRelay3	= RSA.generate(2048)
keyRelay4	= RSA.generate(2048)

emitterPk 	= keyEmitter.publickey().exportKey('DER')

relay1PK	= keyRelay1.publickey().exportKey('DER')
relay2PK	= keyRelay2.publickey().exportKey('DER')
relay3PK	= keyRelay3.publickey().exportKey('DER')
relay4PK	= keyRelay4.publickey().exportKey('DER')

receiverPK  = keyReceiver.publickey().exportKey('DER')

f = open(PATH_PREFIX + "key_emitter", "wb")
f.write(emitterPk)
f.close()

f = open(PATH_PREFIX + "key_relay1", "wb")
f.write(relay1PK)
f.close()

f = open(PATH_PREFIX + "key_relay2", "wb")
f.write(relay2PK)
f.close()

f = open(PATH_PREFIX + "key_relay3", "wb")
f.write(relay3PK)
f.close()

f = open(PATH_PREFIX + "key_relay4", "wb")
f.write(relay4PK)
f.close()

f = open(PATH_PREFIX + "key_receiver", "wb")
f.write(receiverPK)
f.close()

seuil = 4
idPoU = 0
data  = 0

prevPathId = False

while idPoU < seuil:
	print("\033[33mGénération PoU #" + str(idPoU)+ "...\033[00m")
	nonceEmitter = randint(1, 8e9)
	nonceRelay1	 = randint(1, 8e9)
	nonceRelay2	 = randint(1, 8e9)
	nonceRelay3	 = randint(1, 8e9)
	nonceRelay4	 = randint(1, 8e9)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_nonce_emitter', 'wb')
	f.write(pack('!Q', nonceEmitter))
	f.close()

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_nonce_relay1', 'wb')
	f.write(pack('!Q', nonceRelay1))
	f.close()

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_nonce_relay2', 'wb')
	f.write(pack('!Q', nonceRelay2))
	f.close()

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_nonce_relay3', 'wb')
	f.write(pack('!Q', nonceRelay3))
	f.close()

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_nonce_relay4', 'wb')
	f.write(pack('!Q', nonceRelay4))
	f.close()

	nonceHashEmitterObj	= SHA256.new()
	nonceHashRelay1Obj	= SHA256.new()
	nonceHashRelay2Obj	= SHA256.new()
	nonceHashRelay3Obj	= SHA256.new()
	nonceHashRelay4Obj	= SHA256.new()

	nonceHashEmitterObj.update(pack('!Q', nonceEmitter))

	nonceHashRelay1Obj.update(pack('!Q', nonceRelay1))
	nonceHashRelay2Obj.update(pack('!Q', nonceRelay2))
	nonceHashRelay3Obj.update(pack('!Q', nonceRelay3))
	nonceHashRelay4Obj.update(pack('!Q', nonceRelay4))

	nonceHashEmitterBin = nonceHashEmitterObj.digest()

	nonceHashRelay1Bin	= nonceHashRelay1Obj.digest()
	nonceHashRelay2Bin	= nonceHashRelay2Obj.digest()
	nonceHashRelay3Bin	= nonceHashRelay3Obj.digest()
	nonceHashRelay4Bin	= nonceHashRelay4Obj.digest()

	number = idPoU

	timestamp = int(time())

	if prevPathId:
		pathId  = prevPathId

		sEmitter  = Signer.new(keyEmitter)
		sReceiver = Signer.new(keyReceiver)
		sRelay3   = Signer.new(keyRelay3)

		hEmitter = SHA256.new()
		hEmitter.update(pathId + emitterPk)

		hReceiver = SHA256.new()
		hReceiver.update(pathId + receiverPK)

		hRelay3 = SHA256.new()
		hRelay3.update(pathId + relay3PK)

		sEmi = sEmitter.sign(hEmitter)
		sRel = sRelay3.sign(hRelay3)
		sRec = sReceiver.sign(hReceiver)

		f = open(PATH_PREFIX + 'del_sig_emitter', 'wb')
		f.write(sEmi)
		f.close()

		f = open(PATH_PREFIX + 'del_sig_receiver', 'wb')
		f.write(sRec)
		f.close()

		f = open(PATH_PREFIX + 'del_sig_relay3', 'wb')
		f.write(sRel)
		f.close()
	else : pathId = b'\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00'

	padding = 1000
	data = data + number * randint(1024, 8192) # Comme ça on est sur que ça ne fait qu'augmenetr


	pou = pack("!IQ", number, timestamp) + pathId + pack("!IQ", padding, data)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_headers', 'wb')
	f.write(pou)
	f.close()

	pou += emitterPk
	pou += nonceHashEmitterBin

	s = Signer.new(keyEmitter)

	h = SHA256.new()
	h.update(pou)

	sigOutEmitter = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_out_emitter', 'wb')
	f.write(sigOutEmitter)
	f.close()

	pou += sigOutEmitter

	pou += relay1PK
	pou += nonceHashRelay1Bin

	s = Signer.new(keyRelay1)

	h = SHA256.new()
	h.update(pou)

	sigOutRelay1 = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_out_relay1', 'wb')
	f.write(sigOutRelay1)
	f.close()

	pou += sigOutRelay1



	pou += relay2PK
	pou += nonceHashRelay2Bin

	s = Signer.new(keyRelay2)

	h = SHA256.new()
	h.update(pou)

	sigOutRelay2 = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_out_relay2', 'wb')
	f.write(sigOutRelay2)
	f.close()

	pou += sigOutRelay2



	pou += relay3PK
	pou += nonceHashRelay3Bin

	s = Signer.new(keyRelay3)

	h = SHA256.new()
	h.update(pou)

	sigOutRelay3 = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_out_relay3', 'wb')
	f.write(sigOutRelay3)
	f.close()

	pou += sigOutRelay3



	pou += relay4PK
	pou += nonceHashRelay4Bin

	s = Signer.new(keyRelay4)

	h = SHA256.new()
	h.update(pou)

	sigOutRelay4 = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_out_relay4', 'wb')
	f.write(sigOutRelay4)
	f.close()

	pou += sigOutRelay4

	pou += receiverPK

	s = Signer.new(keyReceiver)

	h = SHA256.new()
	h.update(pou)

	sigReceiver = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_receiver', 'wb')
	f.write(sigReceiver)
	f.close()

	pou += sigReceiver


	pou += pack('!Q', nonceRelay4)
	s = Signer.new(keyRelay4)

	h = SHA256.new()
	h.update(pou)

	sigBackRelay4 = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_back_relay4', 'wb')
	f.write(sigBackRelay4)
	f.close()

	pou += sigBackRelay4

	pou += pack('!Q', nonceRelay3)
	s = Signer.new(keyRelay3)

	h = SHA256.new()
	h.update(pou)

	sigBackRelay3 = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_back_relay3', 'wb')
	f.write(sigBackRelay3)
	f.close()

	pou += sigBackRelay3

	pou += pack('!Q', nonceRelay2)
	s = Signer.new(keyRelay2)

	h = SHA256.new()
	h.update(pou)

	sigBackRelay2 = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_back_relay2', 'wb')
	f.write(sigBackRelay2)
	f.close()

	pou += sigBackRelay2

	pou += pack('!Q', nonceRelay1)
	s = Signer.new(keyRelay1)

	h = SHA256.new()
	h.update(pou)

	sigBackRelay1 = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_back_relay1', 'wb')
	f.write(sigBackRelay1)
	f.close()

	pou += sigBackRelay1

	pou += pack('!Q', nonceEmitter)

	s = Signer.new(keyEmitter)

	h = SHA256.new()
	h.update(pou)

	sigBackEmitter = s.sign(h)

	f = open(PATH_PREFIX + 'pou_' + str(idPoU) + '_sig_back_emitter', 'wb')
	f.write(sigBackEmitter)
	f.close()

	pou += sigBackEmitter

	if not(prevPathId):
		toId = emitterPk + relay1PK + relay2PK + relay3PK + relay4PK + receiverPK

		h2 = SHA256.new(toId)
		pathHash = h2.digest()

		prevPathId = pathHash[:16]

		f = open(PATH_PREFIX + 'path_id', 'wb')
		f.write(prevPathId)
		f.close()

	# BSON
	if (number == 0):
		toBson = {
			"emitter_key":bson.binary.Binary(emitterPk),
			"receiver_key":bson.binary.Binary(receiverPK),
			"relay_keys":[
				bson.binary.Binary(relay1PK),
				bson.binary.Binary(relay2PK),
				bson.binary.Binary(relay3PK),
				bson.binary.Binary(relay4PK)
			],
			"init_pou" : {
				"timestamp" : bson.int64.Int64(timestamp),
				"data_amount" : bson.int64.Int64(data),
				"next_frame" : padding,
				"number" : idPoU,

				"nonces" : [
					bson.int64.Int64(nonceEmitter),
					bson.int64.Int64(nonceRelay1),
					bson.int64.Int64(nonceRelay2),
					bson.int64.Int64(nonceRelay3),
					bson.int64.Int64(nonceRelay4)],
				"sig_out" : [
					bson.binary.Binary(sigOutEmitter),
					bson.binary.Binary(sigOutRelay1),
					bson.binary.Binary(sigOutRelay2),
					bson.binary.Binary(sigOutRelay3),
					bson.binary.Binary(sigOutRelay4)
				],
				"sig_back" : [
					bson.binary.Binary(sigBackEmitter),
					bson.binary.Binary(sigBackRelay1),
					bson.binary.Binary(sigBackRelay2),
					bson.binary.Binary(sigBackRelay3),
					bson.binary.Binary(sigBackRelay4)
				],
				"sig_receiver" : bson.binary.Binary(sigReceiver)
			}
		}
	else:
		toBson = {
			"emitter_key":emitterPk,
			"receiver_key":receiverPK,
			"relay_keys":[relay1PK,relay2PK,relay3PK,relay4PK],
			"pou" : {
				"path_id" : prevPathId,
				"timestamp" : bson.int64.Int64(timestamp),
				"data_amount" : bson.int64.Int64(data),
				"next_frame" : padding,
				"number" : idPoU,

				"nonces" : [
					bson.int64.Int64(nonceEmitter),
					bson.int64.Int64(nonceRelay1),
					bson.int64.Int64(nonceRelay2),
					bson.int64.Int64(nonceRelay3),
					bson.int64.Int64(nonceRelay4)],
				"sig_out" : [sigOutEmitter,sigOutRelay1,sigOutRelay2,sigOutRelay3,sigOutRelay4],
				"sig_back" : [sigBackEmitter,sigBackRelay1,sigBackRelay2,sigBackRelay3,sigBackRelay4],
				"sig_receiver" : sigReceiver
			}
		}


	bsonFile = bson.BSON.encode(toBson)
	f = open(PATH_PREFIX + "bson_" + str(idPoU), "wb")
	f.write(bsonFile)
	f.close()


	print("\033[32mPoU #" + str(idPoU)+ " Générée\033[00m")
	idPoU += 1
