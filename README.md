# BALAdIN's Sawtooth transaction processor
This program implements a **transaction processor** for the BALAdIN (formerly BLORK) solution.
The transaction processeur is powered with the [Hyperledger Sawtooth](https://sawtooth.hyperledger.org/docs/core/releases/latest/) solution.

## Dependencies
- Java SDK 1.8
- Maven
- JUnit 5
Java dependencies are listed in the `pom.xml` file.
The official sawtooth package needs to be installed:

```
git clone https://github.com/hyperledger/sawtooth-core
cd sawtooth-core/sdk/java

mvn install
```

Warning : unit tests require a python script to generate cryptographic variables.
Its dependencies are:

- Python 3.5+
- [PyMongo](http://api.mongodb.com/python/)
- [PyCryptoDome](http://pycryptodome.readthedocs.io/en/latest/)

* N.B : Python dependencies are not necessary if unit tests are omitted during compilation (see section "Installation") *

To install sowtooth, [this tutorial](https://sawtooth.hyperledger.org/docs/core/releases/latest/app_developers_guide/docker.html) should work.

## Installation
With unit tests (takes a long time, but necessary if the code is modified)
```
mvn clean package
```

Without unit tests (faster)

```
mvn clean package -DskipTests
```

## Usage
Once the compilation is finished:
```
cd target/
java -jar target/BLORK-0.0.1-SNAPSHOT-jar-with-dependencies.jar tcp://127.0.0.1:4004
```
